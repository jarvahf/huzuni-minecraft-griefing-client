package net.halalaboos.client.ui.simple;

import org.lwjgl.input.Keyboard;

import net.halalaboos.client.base.Binding;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventKeyPressed;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.Gui;

public class Paged extends Gui {

	private int currentPage = 0;
	private PageBind rightBind, leftBind;
	public Paged() {
		super(Type.PAGED);
		rightBind = new PageBind(Keyboard.KEY_RIGHT);
		leftBind = new PageBind(Keyboard.KEY_LEFT);
	}

	@Override
	public void render(int screenWidth, int screenHeight) {
		int 
		yPos = 15,
		xPos = 4,
		count = 0,
		width = 0;
		int[] dimensions = getDimensions();
		
		mc.theClient.getRenderUtils().drawHalRect(xPos - 2, yPos - 2, xPos + dimensions[0] + 3, yPos + dimensions[1] + 12, false, false);
		mc.fontRenderer.drawStringWithShadow("Huzuni", 2, 2, 0xffffff);
		for(Mod mod : ModManager.getMods()) {
			if(mod.isRenderIngame() && !mod.getKeyName().equals("-1")) {
				if(getSelectedPage(count) == currentPage) {
					//Should we use different colors?
					mc.theClient.getGUIFR().drawStringWithShadow(mod.getKeyName() + ":" + mod.getName(), xPos, yPos, mod.isState() ? 0x339900:0xFF0000);
					yPos += mc.theClient.getGUIFR().getFont().getStringHeight("|");
				}
			count++;
			}
		}
		String renderCount = currentPage + "/" + getPageCount();
		mc.theClient.getGUIFR().drawString(renderCount, xPos + dimensions[0] / 2 - (mc.theClient.getGUIFR().getStringWidth(renderCount) / 2), yPos + 1, 0xFFFFFF);
	}
	
	private int getPageCount() {
		return (int) getActualModCount() / mc.theClient.getGUIFR().getFont().getStringHeight("|");
	}
	
	private int getSelectedPage(int count) {
		return (int) count / mc.theClient.getGUIFR().getFont().getStringHeight("|");
	}
	
	private int getActualModCount() {
		int size = 0;
		for(Mod mod : ModManager.getMods()) {
			if(mod.isRenderIngame() && !mod.getKeyName().equals("-1")) {
				size ++;
			}
		}
		return size;
	}
	
	private int[] getDimensions() {
		int yPos = 0, count = 0, width = 0;
		for(Mod mod : ModManager.getMods()) {
			if(mod.isRenderIngame() && !mod.getKeyName().equals("-1")) {
				if(getSelectedPage(count) == currentPage) {
					yPos += mc.theClient.getGUIFR().getFont().getStringHeight("|");
				}
				if(mc.theClient.getGUIFR().getStringWidth(mod.getKeyName() + ":" + mod.getName()) > width)
					width = mc.theClient.getGUIFR().getStringWidth(mod.getKeyName() + ":" + mod.getName());
				count++;
			}
		}
		return new int[] { width, yPos };
	}
	
	private void move(int direction) {
		currentPage += direction;
		int size = getPageCount();
		if(currentPage > size)
			currentPage = 0;
		if(currentPage < 0)
			currentPage = size;
	}
	
	class PageBind extends Binding {

		public PageBind(int keyCode) {
			super("no", keyCode);
			mc.theClient.getEventHandler().registerListener(EventKeyPressed.class, this);
		}

		@Override
		public void onPressed() {
			if(getKeyCode() == Keyboard.KEY_RIGHT)
				move(1);
			else if(getKeyCode() == Keyboard.KEY_LEFT)
				move(-1);
		}
		
	}

}
