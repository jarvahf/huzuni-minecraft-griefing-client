package net.halalaboos.client.ui.tabbed.components;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.Client;

public class Tab {
	protected List<Button> buttons;
	protected String title;
	protected int selectedButton = 0;
	protected boolean opened;
	public Tab(String title) {
		this.title = title;
		buttons = new ArrayList<Button>();
	}
	
	public void render(boolean selected, int posX, int posY, int width) {
		Client.getRenderUtils().drawRect(posX, posY, posX + width, posY + 13, 0x99000000);
		Client.getMC().fontRenderer.drawStringWithShadow(selected ? "\247a" + title : title, posX + 3, posY + 3, 0xFFFFFF);
		Client.getMC().fontRenderer.drawStringWithShadow(opened ? "\247a<<" : selected ? "\247a>>" : ">>", posX + width - 
				Client.getMC().fontRenderer.getStringWidth(">>") - 1, posY + 3, 0xFFFFFF);
		if(selected && opened) {
			for(Button button : buttons) {
				int index = buttons.indexOf(button);
				button.render(selectedButton == index, posX + width + 4, posY + index * 13 + 1, width);
			}
		}
	}
	
	public void recieveInput(String input) {
		if(input.equalsIgnoreCase("UP"))
			movePosition(-1);
		else if(input.equalsIgnoreCase("DOWN"))
			movePosition(1);
		else if(input.equalsIgnoreCase("ENTER"))
			buttons.get(selectedButton).toggle();
		else if(input.equalsIgnoreCase("LEFT"))
			opened = false;
	}
	
	public boolean isEmpty() {
		return buttons.size() == 0;
	}
	
	public void movePosition(int ammount) {
		selectedButton += ammount;
		
		if(selectedButton > buttons.size() - 1)
			selectedButton = 0;
		else if(selectedButton < 0)
			selectedButton = buttons.size() - 1;
	}
	
	public boolean isOpened() {
		return opened;
	}
	
	public void toggleOpen() {
		opened = !opened;
	}
	
	public void add(Button button) {
		buttons.add(0, button);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Button> getButtons() {
		return buttons;
	}
	
}
