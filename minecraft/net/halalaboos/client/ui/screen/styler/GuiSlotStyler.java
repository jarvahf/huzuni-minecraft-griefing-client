package net.halalaboos.client.ui.screen.styler;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection;
import org.lwjgl.opengl.GL11;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Account;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSlot;
import net.minecraft.src.Tessellator;
import net.halalaboos.client.ui.*;
class GuiSlotStyler extends GuiSlot {
	
	private Minecraft mc = Client.getMC();
	private List list;
	private GuiScreen screen;
	private int currentSlot = -1;

	public GuiSlotStyler(GuiScreen par1GuiLanguage) {
		super(Client.getMC(), par1GuiLanguage.width,
				par1GuiLanguage.height, 32, (par1GuiLanguage.height - 65) + 4,
				14);

		screen = par1GuiLanguage;
		setList(net.halalaboos.client.ui.Gui.getGuis());
	}

	/**
	 * Gets the size of the current slot list.
	 */
	protected int getSize() {
		return getList().size();
	}
	
	/**
	 * the element in the slot that was clicked, boolean for whether it was
	 * double clicked or not
	 */
	protected void elementClicked(int par1, boolean par2) {
		currentSlot = par1;

		if (par2 && par1 < getList().size()) {
			Gui gui = ((Gui) getList().get(par1));
			GuiManager.selectGUIStyle(gui.getType());
		}
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	protected boolean isSelected(int par1) {
		return par1 == currentSlot;
	}

	/**
	 * return the height of the content being scrolled
	 */
	protected int getContentHeight() {
		return getSize() * 14;
	}

	protected void drawBackground() {
		screen.drawDefaultBackground();
	}

	protected void drawSlot(int par1, int par2, int par3, int par4,
			Tessellator par5Tessellator) {
		Gui gui = ((Gui) getList().get(par1));
		screen.drawCenteredString(mc.fontRenderer, gui.getType().getName(), screen.width / 2, par3 + 1, GuiManager.getSelectedGUI().getType().equals(gui.getType()) ? 0x3333FF:0x3399FF);
	}

	public int getCurrentSlot() {
		return currentSlot;
	}

	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}

	public void setList(List list) {
		this.list = list;
	}

	public List getList() {
		return list;
	}
}
