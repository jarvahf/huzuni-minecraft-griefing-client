package net.halalaboos.client.ui.screen;

import java.awt.Color;

import org.lwjgl.input.Mouse;
import net.halalaboos.client.manager.WindowManager;
import net.halalaboos.client.utils.SnowEngine;
import net.halalaboos.client.utils.Texture;
import net.minecraft.src.Chunk;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.MathHelper;

public class PanelScreen extends GuiScreen {
		
	public PanelScreen() {
		super();
		WindowManager.getModWindows().updateWindows();
	}

	public void drawScreen(int par1, int par2, float par3) {
		this.drawDefaultBackground();
		super.drawScreen(par1, par2, par3);
		WindowManager.getModWindows().renderWindows(par1, par2);
	}

	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
		WindowManager.getModWindows().onMouseClicked(par1, par2);
	}

	protected void keyTyped(char par1, int par2) {
		super.keyTyped(par1, par2);
		WindowManager.getModWindows().onKeyTyped(par1, par2);
	}
}
