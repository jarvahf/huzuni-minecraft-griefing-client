package net.halalaboos.client.ui.screen.shop;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.lwjgl.Sys;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Account;
import net.halalaboos.client.utils.Connection;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class GuiShop extends GuiScreen {
	protected GuiScreen parentGui;
	private GuiSlotShop shopList;

	public GuiShop(GuiScreen par1GuiScreen) {
		parentGui = par1GuiScreen;
	}

	public void initGui() {
		StringTranslate stringtranslate = StringTranslate.getInstance();
		buttonList.add(new GuiSmallButton(6, width / 2 - 75, height - 22, "Done"));
		shopList = new GuiSlotShop(this);
		shopList.registerScrollButtons(buttonList, 7, 8);
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (!par1GuiButton.enabled) {
			return;
		}

		switch (par1GuiButton.id) {
		case 6:
			mc.displayGuiScreen(parentGui);
			break;

		case 0:
			break;
			
		case 1:
			if(shopList != null) {
				if (shopList.getCurrentSlot() >= 0 && shopList.getList().size() > 0)
					shopList.elementClicked(shopList.getCurrentSlot(), true);
			}
			break;

		default:
			shopList.actionPerformed(par1GuiButton);
			break;
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	protected void keyTyped(char par1, int par2) {
		super.keyTyped(par1, par2);
	}

	/**
	 * Called when the mouse is clicked.
	 */
	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
	}

	/**
	 * Called when the mouse is moved or a mouse button is released. Signature:
	 * (mouseX, mouseY, which) which==-1 is mouseMove, which==0 or which==1 is
	 * mouseUp
	 */
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int par1, int par2, float par3) {
		if(shopList != null)
			shopList.drawScreen(par1, par2, par3);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		drawCenteredString(fontRenderer, "Download Mods", width / 2, 16,
				0xffffff);
		super.drawScreen(par1, par2, par3);
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
		super.updateScreen();
	}

	public void onGuiClosed() {

	}
}
