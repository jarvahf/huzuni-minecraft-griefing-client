package net.halalaboos.client.ui.screen.shop;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.*;
import java.util.*;
import java.util.jar.JarFile;

import javax.net.ssl.HttpsURLConnection;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.manager.GuiManager;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.utils.Account;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.Connection;
import net.halalaboos.client.utils.ThreadDownload;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSlot;
import net.minecraft.src.Tessellator;
import net.halalaboos.client.ui.*;
class GuiSlotShop extends GuiSlot {
	
	private Minecraft mc = Client.getMC();
	private List list;
	private GuiScreen screen;
	private int currentSlot = -1;
	ThreadDownload downloadThread;
	
	public GuiSlotShop(GuiScreen guiScreen) {
		super(Client.getMC(), guiScreen.width,
				guiScreen.height, 32, (guiScreen.height - 65) + 4,
				28);

		screen = guiScreen;
		try {
			setList(getMods());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the size of the current slot list.
	 */
	protected int getSize() {
		return getList().size();
	}
	
	/**
	 * the element in the slot that was clicked, boolean for whether it was
	 * double clicked or not
	 */
	protected void elementClicked(int par1, boolean par2) {
		currentSlot = par1;
	}

	/**
	 * returns true if the element passed in is currently selected
	 */
	protected boolean isSelected(int par1) {
		return par1 == currentSlot;
	}

	/**
	 * return the height of the content being scrolled
	 */
	protected int getContentHeight() {
		return getSize() * 28;
	}

	protected void drawBackground() {
		screen.drawDefaultBackground();
	}

	protected void drawSlot(int par1, int par2, int par3, int par4,
			Tessellator par5Tessellator) {
		if(getList().size() <= par1) 
			return;
		String name = ((String) getList().get(par1)).split(":")[0];
		String description = ((String) getList().get(par1)).split(":")[1];
		float percentage = 0;
		Bounds downloadButton = new Bounds(screen.width / 2 + 60, par3 + 1, 50, 22);
		int mouseX = mc.theClient.getClientUtils().getMouseX(),
		mouseY = mc.theClient.getClientUtils().getMouseY();
		
		if(downloadThread != null) {
			if(downloadThread.getPercentage() == 1) {
				downloadThread = null;
				try {
					setList(getMods());
					return;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				if(downloadThread.getOutputFile().getName().equals(name + ".jar")) {
					percentage = downloadThread.getPercentage();
				}
			}
		}
		
		mc.fontRenderer.drawString(name, 2, par3 + 1, 0xFFFFFF);
		mc.fontRenderer.drawString(description, 2, par3 + 11, 0xFFFFFF);
		if(downloadButton.isPointInside(mouseX, mouseY)) {
			if(Mouse.isButtonDown(0)) {
				if(downloadThread == null) {
					downloadMod(par1);
				}
			}
			mc.theClient.getRenderUtils().drawRect(downloadButton.getX(), downloadButton.getY(), downloadButton.getX() + downloadButton.getWidth(), downloadButton.getY() + downloadButton.getHeight(), currentSlot == par1 ? 0x9f00B3FF : 0x9f5E5E5E);
		} else
			mc.theClient.getRenderUtils().drawRect(downloadButton.getX(), downloadButton.getY(), downloadButton.getX() + downloadButton.getWidth(), downloadButton.getY() + downloadButton.getHeight(), currentSlot == par1 ? 0x8f00B3FF : 0x8f5E5E5E);
		screen.drawCenteredString(mc.fontRenderer, "Download", (int) downloadButton.getX() + (int) downloadButton.getWidth() / 2, (int) downloadButton.getY() + 8, 0xFFFFFF);
	
	}

	private void downloadMod(int listIndex) {
		String name = ((String) getList().get(listIndex)).split(":")[0];
		String url = ((String) getList().get(listIndex)).split(":")[2];
		if(!(url.startsWith("http://")))
			url = "http://" + url;
		try {
			downloadThread = new ThreadDownload(new URL(url), new File(mc.theClient.getClientUtils().getSaveDirectory() + "/mods", name + ".jar"), true);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	
	}
	
	private List<String> getMods() throws IOException {
		Connection connection = new Connection(new URL("http://halalaboos.net/client/external/mods"));
		connection.createConnection();
		List<String> tempList = new ArrayList<String>();
		for(String s : connection.readURL()) {
			if(s.split(":").length >= 3) {
				String name = s.split(":")[0];
				if(ModManager.getMod(name) == null)
					tempList.add(s);
			}
		}
		return tempList;
	}
	
	public int getCurrentSlot() {
		return currentSlot;
	}

	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}

	public void setList(List list) {
		this.list = list;
	}

	public List getList() {
		return list;
	}
}
