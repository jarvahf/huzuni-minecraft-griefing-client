package net.halalaboos.client.ui.screen;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import net.halalaboos.client.Client;
import net.halalaboos.client.files.SettingsFile;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;
import org.lwjgl.input.Keyboard;

public class GuiFirstTime extends GuiScreen {

	private GuiTextField usernameField;
	private GuiScreen lastScr;

	public GuiFirstTime() {
		super();
		lastScr = new GuiMainMenu();
	}

	public GuiFirstTime(GuiScreen b) {
		super();
		lastScr = b;
	}

	public void updateScreen() {
		usernameField.updateCursorCounter();
	}

	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	protected void actionPerformed(GuiButton guibutton) {
		if (guibutton.id == 0) {
			SettingsFile.setUsername(this.usernameField.getText().trim());
			mc.displayGuiScreen(new Screen2());
		}
	}

	protected void keyTyped(char c, int i) {
		usernameField.textboxKeyTyped(c, i);
		if (c == '\r') {
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	protected void mouseClicked(int i, int j, int k) {
		super.mouseClicked(i, j, k);
		usernameField.mouseClicked(i, j, k);
	}

	GuiButton c;

	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();
		usernameField = new GuiTextField(fontRenderer, width / 2 - 100, 76,
				200, 20);
		usernameField.setFocused(true);
		usernameField.setMaxStringLength(44);
		usernameField.setText(SettingsFile.getUsername());
		c = new GuiButton(0, width / 2 - 2 - 48, height / 4 + 96 - 12, 98, 20,
				"Next");
		c.enabled = usernameField.getText().length() > 0;
		buttonList.add(c);
	}

	public void drawScreen(int i, int j, float f) {
		drawDefaultBackground();
		drawCenteredString(fontRenderer, "Welcome to " + Client.CLIENT_TITLE,
				width / 2, (height / 4 - 60) + 20, 0xffffff);
		drawString(fontRenderer, "Set your username here.", width / 2 - 100,
				63, 0xa0a0a0);
		drawString(fontRenderer,
				"The client will now refer to you by this alias.",
				width / 2 - 100, 104, 0xa0a0a0);
		usernameField.drawTextBox();
		c.enabled = usernameField.getText().length() > 0;
		super.drawScreen(i, j, f);
	}

	/**
	 * Second screen;
	 * */
	public class Screen2 extends GuiScreen {

		protected void actionPerformed(GuiButton guibutton) {
			if (guibutton.id == 0)
				mc.displayGuiScreen(lastScr);
				//mc.displayGuiScreen(new Screen3());

		}

		protected void keyTyped(char c, int i) {
			if (c == '\r') {
				actionPerformed((GuiButton) buttonList.get(0));
			}
		}

		protected void mouseClicked(int i, int j, int k) {
			super.mouseClicked(i, j, k);
		}

		public void initGui() {
			Keyboard.enableRepeatEvents(true);
			buttonList.clear();
			buttonList.add(new GuiButton(0, width / 2 - 2 - 48,
					height / 4 + 96 - 12, 98, 20, "Continue"));
		}

		public void drawScreen(int i, int j, float f) {
			drawDefaultBackground();
			int y = 63;
			String[][] peeps = new String[][] {
					{"TheObliterator", "Font rendering engine, heavily modified"},
					{"sp614x", "Optifine"},
					{"Jonalu", "Scissor box method"},
			};
			for(String[] s : peeps) {
				drawString(fontRenderer,
						"\2472" + s[0] + "\247f - " + s[1],
						width / 2 - 100, y, 0xffffff);
				y+=10;
			}
			drawCenteredString(fontRenderer, "Credits!", width / 2,
					(height / 4 - 60) + 20, 0xffffff);

			super.drawScreen(i, j, f);
		}
	}
	
	/**
	 * Third screen;
	 * */
	public class Screen3 extends GuiScreen {
		GuiButton select;
		
		protected void actionPerformed(GuiButton guibutton) {
			if (guibutton.id == 0)
				mc.displayGuiScreen(lastScr);

		}

		protected void keyTyped(char c, int i) {
			if (c == '\r') {
				actionPerformed((GuiButton) buttonList.get(0));
			}
		}

		protected void mouseClicked(int i, int j, int k) {
			super.mouseClicked(i, j, k);
		}

		public void initGui() {
			Keyboard.enableRepeatEvents(true);
			buttonList.clear();
			select = new GuiButton(0, width / 2 - 2 - 48,
					height / 4 + 96 - 12, 98, 20, "Select");
			buttonList.add(select);
			select.enabled = false;
		}

		public void drawScreen(int i, int j, float f) {
			drawDefaultBackground();
			drawCenteredString(fontRenderer, "What GUI style do you prefer?", width / 2,
					(height / 4 - 60) + 20, 0xffffff);
			super.drawScreen(i, j, f);
		}
	}
}
