package net.halalaboos.client.ui.screen.alts;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.lwjgl.Sys;

import net.halalaboos.client.Client;
import net.halalaboos.client.files.AccountFile;
import net.halalaboos.client.ui.screen.ButtonPoop;
import net.halalaboos.client.utils.Account;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class GuiAlts extends GuiScreen {
	protected GuiScreen parentGui;
	private GuiSlotAlts altList;
	public GuiTextField field;

	public GuiAlts(GuiScreen par1GuiScreen) {
		parentGui = par1GuiScreen;
	}

	GuiButton use;
	GuiButton remove;

	public void initGui() {
		field = new GuiTextField(mc.fontRenderer, width / 2 - 75 - 35, height - 60, 220, 14);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		buttonList.add(new ButtonPoop(6, width / 2 - 75 - 39, height - 22, 227, 20, "Done"));

		altList = new GuiSlotAlts(this);
		altList.registerScrollButtons(buttonList, 7, 8);
		buttonList.add(new ButtonPoop(15, 2, 2, 75, 20, "Open Alts.."));

		buttonList.add(new ButtonPoop(12, width / 2 - 38, height - 44, 75, 20, "Add"));

		use = new ButtonPoop(14, width / 2 - 75 - 39, height - 44, 75, 20, "Login");
		buttonList.add(use);
		use.enabled = altList.getCurrentSlot() >= 0;
		
		remove = new ButtonPoop(13, width / 2 + 38, height - 44, 75, 20, "Remove");
		buttonList.add(remove);
		remove.enabled = altList.getCurrentSlot() >= 0;
	}

	/**
	 * Fired when a control is clicked. This is the equivalent of
	 * ActionListener.actionPerformed(ActionEvent e).
	 */
	protected void actionPerformed(GuiButton par1GuiButton) {
		if (!par1GuiButton.enabled) {
			return;
		}

		switch (par1GuiButton.id) {
		case 6:
			mc.displayGuiScreen(parentGui);
			break;

		case 12:
			mc.displayGuiScreen(new GuiLogin(this, true));
			break;
		case 13:
			altList.removeAccount();
			break;
		case 14:
			if (altList.getCurrentSlot() >= 0 && altList.getList().size() > 0)
				altList.elementClicked(altList.getCurrentSlot(), true);
			break;
		case 15:
			new Thread(new Runnable() {
				public void run() {
					JFileChooser fileChooser = new JFileChooser();
					if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						try {
							BufferedReader reader = new BufferedReader(new FileReader(selectedFile));
							for(String s; (s = reader.readLine()) != null; ) {
								if(s.contains(":")) {
									Account account = new Account(s.split(":")[0], s.split(":")[1], true);
									if(!AccountFile.contains(account))
										AccountFile.getAccounts().add(account);
								}
							}
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
 			}).start();
			break;
		default:
			altList.actionPerformed(par1GuiButton);
			break;

		case 5:
			break;
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	protected void keyTyped(char par1, int par2) {
		super.keyTyped(par1, par2);
		field.textboxKeyTyped(par1, par2);

		if (field.getText().length() > 0) {
			ArrayList l = new ArrayList();
			for (Account a : AccountFile.getAccounts()) {
				if (a.getUsername().toLowerCase()
						.startsWith(field.getText().toLowerCase().trim()))
					l.add(a);
			}
			altList.setList(l);
		} else
			altList.setList(AccountFile.getAccounts());
	}

	/**
	 * Called when the mouse is clicked.
	 */
	protected void mouseClicked(int par1, int par2, int par3) {
		super.mouseClicked(par1, par2, par3);
		field.mouseClicked(par1, par2, par3);
	}

	/**
	 * Called when the mouse is moved or a mouse button is released. Signature:
	 * (mouseX, mouseY, which) which==-1 is mouseMove, which==0 or which==1 is
	 * mouseUp
	 */
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	public void drawScreen(int par1, int par2, float par3) {
		String current = "Current Account: " + mc.session.username
				+ " \247c";
		altList.drawScreen(par1, par2, par3);
		// lastAltList.drawScreen(par1, par2, par3);
		StringTranslate stringtranslate = StringTranslate.getInstance();
		Client.getButtonFR().drawCenteredString("Change account.", width / 2, 16,
				0xffffff);
		super.drawScreen(par1, par2, par3);
		Client.getFR().drawString(current, this.width-Client.getFR().getStringWidth(current), 2, 0xffffff);
		Client.getFR().drawString(
				"Amount of alts: " + (AccountFile.getAccounts().size()), 2,
				height - 12, 0xffffff);
		use.enabled = altList.getCurrentSlot() >= 0;
		remove.enabled = altList.getCurrentSlot() >= 0;
		field.drawTextBox();
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	public void updateScreen() {
		super.updateScreen();
		field.updateCursorCounter();
	}

	public void onGuiClosed() {

	}
}
