package net.halalaboos.client.ui.window.theme;

import net.halalaboos.client.Client;
import net.halalaboos.client.ui.window.WindowTheme;
import net.halalaboos.client.utils.RenderUtils;
import net.halalaboos.client.utils.Texture;

public class Custom extends WindowTheme {
	private RenderUtils rUtils = Client.getRenderUtils();
	
	@Override
	public void renderElement(double x, double y, double width, double height,  boolean mouseAround, boolean isEnabled) {
		drawButtonRect(x, y, x + width, y + height, mouseAround, isEnabled);
	}

	@Override
	public void renderPanel(double x, double y, double width, double height) {
		rUtils.drawBorderedRect(x, y, x + width, y + height, 1f, 0.1F,  0.1F,  0.1F, 0.75f);
	}

	public void drawButtonRect(double x, double y, double x1, double y1,
			boolean b, boolean e) {
		if (e) {
			if (b)
				rUtils.drawBorderedRect(x, y, x1, y1, 1f,  0.9f, 0.2f, 0.2f, 0.75f);
			else
				rUtils.drawBorderedRect(x, y, x1, y1, 1f,  0.8f, 0.3f, 0.3f, 0.75f);
		} else {
			if (b)
				rUtils.drawBorderedRect(x, y, x1, y1, 1f, 0.5f, 0.5f, 0.5f, 0.75f);
			else
				rUtils.drawBorderedRect(x, y, x1, y1, 1f, 0.3f, 0.3f, 0.3f, 0.75f);
		}
	}

	@Override
	public void renderToolTip(String text, double x, double y) {
		double width =  Client.getGUIFR().getStringWidth(text) + 4,
		height = 12;
		Client.getRenderUtils().drawRect(x, y, x + width, y + height, 0, 0, 0, 0.3f);
		Client.getGUIFR().drawStringWithShadow(text, x + 2, y + 2, 0xffffbb);		
	}
}
