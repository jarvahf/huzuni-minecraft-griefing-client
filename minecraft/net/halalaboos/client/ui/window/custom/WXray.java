package net.halalaboos.client.ui.window.custom;

import net.halalaboos.client.files.XrayFile;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.window.WElement;
import net.halalaboos.client.ui.window.WManager;
import net.halalaboos.client.ui.window.Window;
import net.halalaboos.client.ui.window.elements.ItemButton;

public class WXray extends Window {

	public WXray(WManager manager) {
		super("Xray Manager", 2, 2, 0, 0, manager);
		// TODO Auto-generated constructor stub
	}
	
	public void init() {
		int x = 2;
		int y = 14;
		Integer[] ids = new Integer[] { 56, 14, 15, 16, 9, 11, 42, 46, 48, 49,
				57, 129 };
		for (int i : ids) {
			ItemButton b = new ItemButton(i, x, y, 20, 20);
			b.setEnabled(XrayFile.contains(i));
			add(b);
			x += 21;
			if (x / 21 > 5) {
				x = 2;
				y += 21;
			}
		}
	}
	
	public void onObjectClicked(WElement o) {
		super.onObjectClicked(o);
		if (o instanceof ItemButton) {
			ItemButton b = (ItemButton) o;
			tacoSauce(b.getItemID(), XrayFile.contains(b.getItemID()));
			b.setEnabled(XrayFile.contains(b.getItemID()));
		}
		mc.sndManager.playSoundFX("random.click", 1.0F, 1.0F);
		if (ModManager.getMod("Xray").isState())
			mc.renderGlobal.loadRenderers();
	}

	public void tacoSauce(int blockID, boolean burritoSauce) {
		if (burritoSauce)
			XrayFile.remove(blockID);
		else
			XrayFile.add(blockID);
	}
	
	public void updatePanel() {
		for (WElement o : objects) {
			if (o instanceof ItemButton) {
				ItemButton b = (ItemButton) o;
				b.setEnabled(XrayFile.contains(b.getItemID()));
			}
		}
	}
}
