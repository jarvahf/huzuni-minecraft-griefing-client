package net.halalaboos.client.ui.window;

import java.awt.*;
import java.util.*;
import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.ui.window.elements.Button;
import net.halalaboos.client.ui.window.theme.Custom;
import net.halalaboos.client.ui.window.theme.Default;
import net.halalaboos.client.utils.Bounds;
import net.halalaboos.client.utils.Counter;
import net.halalaboos.client.utils.RenderUtils;
import net.minecraft.client.*;
import org.lwjgl.input.*;

public class Window {
	public Minecraft mc = Client.getMC();
	public Client cl = Client.getInstance();
	
	public ArrayList<WElement> objects = new ArrayList<WElement>();
	private String name;
	private int x, y, width, height,// positioning for rendering
			mXOff, mYOff, xOffset, yOffset, widthOffset, heightOffset, lastMouseY, lastMouseX; // for dragging the panel
	private boolean dragging, resizing, allowResizing, allowClosing, closed, keepPanelFormatted, pinned = false, pinnable, draggableBody = true;
	private WElement mousedObject = null;
	private WManager manager;
	private WindowTheme wTheme;
	private Counter tooltipCounter = new Counter(750, 0);

	public Window(String n, int x, int y, int width, int height, WManager manager) {
		this.name = n;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.manager = manager;
		wTheme = new Default();
		init();
		formatPanel();
		setClosable(true);
		setClosed(true);
	}

	public void init() {}

	public void render(int mouseX, int mouseY) {

		dragWindow(mouseX, mouseY);
		
		if (isResizable())
			resizeWindow(mouseX, mouseY);
		
		Color c = getManager().getPrimaryColor();
		wTheme.renderPanel(x, y, width, 13);
		cl.getGUIFR().drawStringWithShadow("\247l" + getName(), x + 2, y + 3,
				0xffffff);

		if (shouldRenderBody()) {
			wTheme.renderPanel(x, y + 12, width, height - 12);
			drawPanelObjects(mouseX, mouseY);
		}
		
		Bounds resizer = getResizer();
		Bounds closeButton = getCloseButton();
		Bounds pinButton = getPinButton();

		if (isResizable() && shouldRenderBody() && (getBody().isPointInside(mouseX, mouseY)|| getResizer().isPointInside(mouseX, mouseY) || resizing))
			wTheme.renderElement(
					resizer.getX(),
					resizer.getY(),
					resizer.getWidth(),
					resizer.getHeight(),
					resizer.isPointInside(mouseX, mouseY),
					resizer.isPointInside(mouseX, mouseY)
							&& Mouse.isButtonDown(0));

		if (isClosable())
			wTheme.renderElement(closeButton.getX(), 
					closeButton.getY(), 
					closeButton.getWidth(), 
					closeButton.getHeight(),
					closeButton.isPointInside(mouseX, mouseY), !isClosed());

		if (isPinnable())
			wTheme.renderElement(pinButton.getX(), pinButton.getY(), 
					pinButton.getWidth(),
					pinButton.getHeight(),
					pinButton.isPointInside(mouseX, mouseY), isPinned());
		
	}

	public boolean shouldRenderBody() {
		if (isClosable())
			return !isClosed();
		else
			return true;
	}

	public boolean onClick(int mouseX, int mouseY) {
		if (isClosable()) {
			if (getCloseButton().isPointInside(mouseX, mouseY)) {
				setClosed(!isClosed());
				return true;
			}
		}
		
		if (isPinnable()) {
			if (getPinButton().isPointInside(mouseX, mouseY)) {
				setPinned(!isPinned());
				return true;
			}
		}
		if (shouldRenderBody()) {
			for(int o = objects.size() - 1; o >= 0; o--) {
				WElement element = objects.get(o);
				if (element.onMouseClicked(this, mouseX, mouseY)) {
					getManager().updateWindows();
					return true;
				}
			}
			if(!getTop().isPointInside(mouseX, mouseY) && (getBody().isPointInside(mouseX, mouseY)
					&& !isDraggableBody())) {
				return true;
			}
		}

		if (getResizer().isPointInside(mouseX, mouseY) && isResizable()
				&& shouldRenderBody()) {
			mXOff = mouseX;
			mYOff = mouseY;
			widthOffset = width;
			heightOffset = height;
			resizing = true;
			return true;
		} else
			resizing = false;

		if (getTop().isPointInside(mouseX, mouseY) || (getBody().isPointInside(mouseX, mouseY)
				&& shouldRenderBody())) {
			mXOff = mouseX;
			mYOff = mouseY;
			xOffset = x;
			yOffset = y;
			dragging = true;
			return true;
		} else
			dragging = false;

		return false;
	}

	public void onObjectClicked(WElement element) {
		if (element instanceof Button) {
			Button b = (Button) element;
			if (ModManager.getMod(b.getText()) != null) {
				ModManager.getMod(b.getText()).toggle();
			} else
				mc.sndManager.playSoundFX("random.click", 1.0F, 1.0F);
		}
		if(isKeepPanelFormatted()) {
			this.formatPanel();
		}
	}

	public void onObjectTyped(WElement element) {}

	public void keyTyped(char c, int keyCode) {
		for (WElement o : objects) {
			o.keyTyped(this, c, keyCode);
		}
	}

	private void dragWindow(int mouseX, int mouseY) {
		if (dragging && Mouse.isButtonDown(0)) {
			x = mouseX - (mXOff - xOffset);
			y = mouseY - (mYOff - yOffset);
		} else
			dragging = false;
	}

	private void resizeWindow(int mouseX, int mouseY) {
		if (resizing && Mouse.isButtonDown(0)) {
			width = (mouseX - (mXOff - widthOffset));
			height = (mouseY - (mYOff - heightOffset));
			keepPanelSafe();
			if(width < cl.getGUIFR().getStringWidth(getName()) + 29)
				width = cl.getGUIFR().getStringWidth(getName()) + 29;
			if(height < 14)
				height = 14;
		} else
			resizing = false;
	}

	public Bounds getBody() {
		return new Bounds(getX(), // x
				getY(), // y
				getWidth(), // x1
				getHeight()// y1
		);
	}

	public Bounds getTop() {
		return new Bounds(getX(), // x
				getY(), // y
				getWidth(), // x1
				13// y1
		);
	}

	public Bounds getResizer() {
		double offset = 3;
		return new Bounds(getX() + getWidth() - offset, // x
				getY() + getHeight() - offset, // y
				offset * 2, // x1
				offset * 2// y1
		);
	}

	public Bounds getCloseButton() {
		return new Bounds(getX() + getWidth() - 11, // x
				getY() + 2, // y
				9, // x1
				9 // y1
		);
	}
	
	public Bounds getPinButton() {
		return new Bounds(getX() + getWidth() - 21, // x
				getY() + 2, // y
				9, // x1
				9 // y1
		);
	}

	public void add(WElement object) {
		this.objects.add(object);
		object.setWindow(this);
		if (object.getObjectWidth() > width)
			width = object.getObjectWidth();
		if (object.getObjectHeight() > height)
			height = object.getObjectHeight();
	}
	
	public boolean mouseOverBody(int mouseX, int mouseY) {
		return (getBody().isPointInside(mouseX, mouseY) && shouldRenderBody());
	}
	
	public void drawPanelObjects(int mouseX, int mouseY) {
		if(mousedObject != null) {
			if(!mousedObject.getObjectBounds(this).isPointInside(mouseX, mouseY)) {
				mousedObject = null;
				tooltipCounter.reset();
			}
		}
		
		for (WElement o : objects) {
			if(o.getObjectBounds(this).isPointInside(mouseX, mouseY) && mousedObject == null) {
				if(!getManager().isWindowOverObject(this, o) && !isObjectOverObject(o, mouseX, mouseY))
					mousedObject = o;
			}
			o.render(this, wTheme, mousedObject == o);
		}
		
		if(mousedObject != null) {
			String text = mousedObject.getToolTipText(this, getTheme());
			if(text != null) {
				tooltipCounter.count();
				if(tooltipCounter.getCount() > 1) {
					getTheme().renderToolTip(text, mouseX + 2, mouseY + 6);
				}
			}
		}
	}
	
	public boolean isObjectOverObject(WElement currentObject, int mouseX, int mouseY) {
		for(WElement o : objects) {
			if((currentObject.getObjectBounds(this).isPointInside(mouseX, mouseY) && o.getObjectBounds(this).isPointInside(mouseX, mouseY)) && objects.indexOf(o) > objects.indexOf(currentObject))
				return true;
		}
		return false;
	}
	
	public void keepPanelSafe() {
		for (WElement o : objects) {
			if (o.getObjectWidth() > width)
				width = o.getObjectWidth();
			if (o.getObjectHeight() > height)
				height = o.getObjectHeight();
		}
	}

	public void formatPanel() {
		this.setWidth(cl.getGUIFR().getStringWidth(getName()) + 29);
		this.setHeight(16);
		this.keepPanelSafe();
	}
	
	public void updatePanel() {}
	
	public boolean isResizable() {
		return allowResizing;
	}

	public void setResizable(boolean shouldAllowResizing) {
		this.allowResizing = shouldAllowResizing;
	}

	public boolean isClosable() {
		return allowClosing;
	}

	public void setClosable(boolean allowClosing) {
		this.allowClosing = allowClosing;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WManager getManager() {
		return manager;
	}

	public void setManager(WManager manager) {
		this.manager = manager;
	}
	
	public boolean isKeepPanelFormatted() {
		return keepPanelFormatted;
	}

	public void setKeepPanelFormatted(boolean keepPanelFormatted) {
		this.keepPanelFormatted = keepPanelFormatted;
	}

	public boolean isPinned() {
		return pinned;
	}

	public void setPinned(boolean pinned) {
		this.pinned = pinned;
	}

	public boolean isPinnable() {
		return pinnable;
	}

	public void setPinnable(boolean pinnable) {
		this.pinnable = pinnable;
	}

	public WindowTheme getTheme() {
		return wTheme;
	}

	public void setTheme(WindowTheme wTheme) {
		this.wTheme = wTheme;
	}
	
	public boolean isDraggableBody() {
		return draggableBody;
	}

	public void setDraggableBody(boolean draggableBody) {
		this.draggableBody = draggableBody;
	}

	private boolean hasMouseMoved(int mouseX, int mouseY) {
		boolean moved = mouseX - lastMouseX != 0 || mouseY - lastMouseY != 0;
		if(moved) {
			lastMouseX = mouseX;
			lastMouseY = mouseY;
		}
		return moved;
	}
	
}