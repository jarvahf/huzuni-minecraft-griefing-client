package net.halalaboos.client;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.base.Manager;
import net.halalaboos.client.event.EventHandler;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.manager.*;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.ModLoader;
import net.halalaboos.client.mods.SideMod;
import net.halalaboos.client.networking.HuzuniClientImpl;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.ClientUtils;
import net.halalaboos.client.utils.RenderUtils;
import net.halalaboos.client.utils.Texture;
import net.halalaboos.client.utils.TextureUtils;
import net.halalaboos.client.utils.font.CFontRenderer;
import net.minecraft.client.Minecraft;

public class Client {

	/**
	 * GameWindowListener.java, Minecraft.java - onEnd hook.
	 * Minecraft.java - erry thing
	 * GuiMainMenu.java - Login button.
	 * FontRenderer.java - Just saw an odd line of code that was 'var9 = var9'. Had to fix. Made getter for colorcode array.
	 * RenderEngine.java - Modified bindtexture method for my image rendering method to work.
	 * TcpReaderThread.java - Fixed dat thing.
	 * EntityRenderer.java - Render3D event.
	 * RenderLiving.java - RenderNamePlate event.
	 * GuiNewChat.java - RecieceMessage event, custom font rendering.
	 * TcpConnection.java - RecievePacket event.
	 * Block.java, RenderBlocks.java - RenderBlock event.
	 * ThreadConnectToServer.java - Setting current ip when connecting to a server.
	 * GuiIngameMenu.java - Huzuni server button
	 * GuiSlot.java - Lots of shit
	 * GuiScreen.java - Background
	 * BlockFluid.java - makin it so wat0r dun puu.sh
	 * */
	private static Client instance;
	public static final String CLIENT_TITLE = "Huzuni";
	public static final String CLIENT_VERSION = "1.7";
	private static Minecraft mc;
	private static CFontRenderer fontRenderer, buttonFont, updateString, guiFontRenderer;
	private static EventHandler eventHandler;
	private static RenderUtils renderUtils;
	private static ClientUtils clientUtils;
	private static TextureUtils textureUtils;
	private static ModLoader modLoader;
	private static HuzuniClientImpl networkingClient;
	private static List<String> chatQueue = new ArrayList<String>();
	static Texture background;
	
	public Client(Minecraft mc) {
		this.mc = mc;
		instance = this;
		renderUtils = new RenderUtils(mc);
		clientUtils = new ClientUtils(mc);
		
		/**
		 * Throughout the client
		 */
		fontRenderer = new CFontRenderer(mc, "Verdana", 18);
		guiFontRenderer = new CFontRenderer(mc, "Tahoma", 18);
		
		/**
		 * For the main menu
		 */
		buttonFont = new CFontRenderer(mc, "Tahoma", 22);
		updateString = new CFontRenderer(mc, "Tahoma", 16);
		eventHandler = new EventHandler();
		if(background == null)
			background = new Texture("/" + Client.CLIENT_TITLE + "/" + "deadfetus.jpg");
	}
	
	public void onStartup() {
		new ValueManager();
		new CommandManager();
		new ModManager();
		new WindowManager();
		new GuiManager();
		new WaypointManager();
		new FileManager();
		modLoader = new ModLoader();
		for(Manager m : Manager.getManagers()) {
			try{	
				m.onStartup();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		networkingClient = new HuzuniClientImpl("127.0.0.1", 6969);
	}
	
	public void onEnd() {
		for(Manager m : Manager.getManagers()) {
			try{
				m.onEnd();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		networkingClient.stop();
	}

	public void addChatMessage(String msg) {
		if(mc.thePlayer == null) {
			chatQueue.add(msg);
		} else
			mc.thePlayer.addChatMessage(ModManager.getTtfMode().isState() ? "\247l\247q[" + CLIENT_TITLE + "]\247r " + msg : 
				"[\2479\247l" + CLIENT_TITLE + "\247r] " + msg);
	}
	
	public void onConnectToServer(String ip, int port) {
        SettingsFile.setServerIP(ip);
        SettingsFile.setServerPort(port);
		networkingClient.updateConnectionStatus(ip, port);
	}
	
	public void onWorldSpawned() {
		if(SettingsFile.isFirstTime()) {
			addChatMessage("Welcome to Huzuni!");
			addChatMessage("Type '.help' for a list of chat commands.");
			addChatMessage("For a list of keybinds, open up your menu and open the keybind manager.");
			SettingsFile.setFirstTime(false);
		}
		for(String s : chatQueue)
			addChatMessage(s);
		chatQueue.clear();
	}
	
	public void onTick() {
		eventHandler.call(new EventTick(this));
	}
	
	public static EventHandler getEventHandler() {
		return eventHandler;
	}

	public static RenderUtils getRenderUtils() {
		return renderUtils;
	}

	public static ClientUtils getClientUtils() {
		return clientUtils;
	}

	public static TextureUtils getTextureUtils() {
		return textureUtils;
	}
	
	public static CFontRenderer getFR() {
		return fontRenderer;
	}
	
	public static CFontRenderer getButtonFR() {
		return buttonFont;
	}
	
	public static CFontRenderer getUpdateFR(){
		return updateString;
	}
	
	public static CFontRenderer getGUIFR() {
		return guiFontRenderer;
	}
	
	public static void setFR(CFontRenderer fontRenderer) {
		Client.fontRenderer = fontRenderer;
	}

	public static ModLoader getModLoader() {
		return modLoader;
	}

	public static HuzuniClientImpl getNetworkingClient() {
		return networkingClient;
	}
	
	public static Minecraft getMC() {
		return mc;
	}
	
	public static void drawClientBG(){
		background.renderTexture(0, 0, Client.getClientUtils().getScreenWidth(),
				Client.getClientUtils().getScreenHeight(), Color.white);
	}
	
	public static Client getInstance() {
		return instance;
	}
	
}
