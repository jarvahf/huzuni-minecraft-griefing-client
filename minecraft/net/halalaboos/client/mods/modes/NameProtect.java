package net.halalaboos.client.mods.modes;

import net.halalaboos.client.base.Friend;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRecieveMessage;
import net.halalaboos.client.event.events.EventRenderNamePlate;
import net.halalaboos.client.event.events.EventSendMessage;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.files.SettingsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.StringUtils;

public class NameProtect extends Mod implements Listener {

	public NameProtect() {
		super("NameProtect", -1, false);
		setCategory(WindowCategory.MODES);
		setDescription("Replace friends names with aliases");
		setState(true);
		
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRenderNamePlate) {
			EventRenderNamePlate rEvent = (EventRenderNamePlate) event;
			rEvent.setText(FriendsFile.replaceNames(rEvent.getText(), true, "", true));
			
		} else if(event instanceof EventRecieveMessage) {
			EventRecieveMessage mEvent = (EventRecieveMessage) event;
			mEvent.setMessage(mEvent.getMessage().replaceAll(mc.session.username, "\2479" + SettingsFile.getUsername() + "\247f"));
			mEvent.setMessage(FriendsFile.replaceNames(mEvent.getMessage(), true, "", true));
		} else if(event instanceof EventSendMessage) {
			EventSendMessage sEvent = (EventSendMessage) event;
			sEvent.setMessage(FriendsFile.replaceNames(sEvent.getMessage(), false, "-", false));
		}
	}
	
	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRenderNamePlate.class, this);
		eventHandler.registerListener(EventRecieveMessage.class, this);
		eventHandler.registerListener(EventSendMessage.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRenderNamePlate.class, this);
		eventHandler.unRegisterListener(EventRecieveMessage.class, this);
		eventHandler.unRegisterListener(EventSendMessage.class, this);

	}

}
