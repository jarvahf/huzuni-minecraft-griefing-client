package net.halalaboos.client.mods.modes;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventDisplayScreen;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.Bounds;
import net.minecraft.src.Block;
import net.minecraft.src.CraftingManager;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.GuiCrafting;
import net.minecraft.src.GuiInventory;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.IRecipe;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.RenderHelper;
import net.minecraft.src.RenderItem;
import net.minecraft.src.ShapedRecipes;
import net.minecraft.src.ShapelessRecipes;
import net.minecraft.src.Slot;
import net.minecraft.src.World;

public class AutoCrafting extends Mod implements Listener {

    private List tempInventory;
	private List<IRecipe> craftableRecipes;
	private IRecipe selectedRecipe;
	
	public AutoCrafting() {
		super("AutoCrafting", -1);
		setDescription("Automagically craft items. (Buggy)");
		setCategory(WindowCategory.MODES);
		setRenderIngame(false);
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventDisplayScreen) {
			EventDisplayScreen sEvent = (EventDisplayScreen) event;
			if(sEvent.getScreen() instanceof GuiInventory) {
				sEvent.setScreen(new FakeGuiInventory(mc.thePlayer, this));
			} else if(sEvent.getScreen() instanceof GuiCrafting) {
				GuiCrafting craftingInventory = (GuiCrafting) sEvent.getScreen();
				sEvent.setScreen(new FakeGuiCrafting(mc.thePlayer, mc.theWorld, mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ, this));
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventDisplayScreen.class, this);
		
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventDisplayScreen.class, this);
		
	}
	
	public void findCraftableRecipes(boolean is2x2) {

		List<IRecipe> craftableRecipes = new ArrayList<IRecipe>();
		tempInventory = copyInventoryStacks();
		for(Object o : CraftingManager.getInstance().getRecipeList()) {
			IRecipe iRecipe = (IRecipe) o;
			if(o instanceof ShapedRecipes) {
				ShapedRecipes recipe = (ShapedRecipes) o;
				boolean isCraftable = true;
				if(is2x2 && (recipe.getRecipeWidth() > 2 || recipe.getRecipeHeight() > 2))
					isCraftable = false;
				for(ItemStack item : recipe.getRecipeItems()) {
					if(item != null) {
						if(!containsInventory(item)) {
							isCraftable = false;	
						}
					}
				}
				
				if(isCraftable)
					craftableRecipes.add(recipe);
			} else if(o instanceof ShapelessRecipes) {
				ShapelessRecipes recipe = (ShapelessRecipes) o;
				boolean isCraftable = true;
				if(is2x2 && recipe.getRecipeSize() > 4)
					isCraftable = false;
				for(Object o1 : recipe.getRecipeItems()) {
					ItemStack item = (ItemStack) o1;
					if(item != null) {
						if(!containsInventory(item))
							isCraftable = false;	
					}
				}
				
				if(isCraftable)
					craftableRecipes.add(recipe);
			}
			tempInventory = copyInventoryStacks();
		}
		this.craftableRecipes = craftableRecipes;
	}
	
	public void renderCraftableItems(int mouseX, int mouseY, int width, int height, boolean is2x2) {
		int posY = 2,
		posX = 2;
		mc.fontRenderer.drawStringWithShadow(getName(), posX, posY, 0xFFFFFF);
		selectedRecipe = null;
		posY += 12;
		for(IRecipe recipe : craftableRecipes) {
			renderItem(recipe.getRecipeOutput(), posX, posY, mouseX, mouseY);
			if(selectedRecipe == null) {
				if(isMouseAround(mouseX, mouseY, posX, posY, 20, 20))
					selectedRecipe = recipe;
			}
			posY += 22;
			if(posY + 22 > height) {
				posY = 14;
				posX += 22;
			}
		}
		if(selectedRecipe != null)
			renderRecipeItems(selectedRecipe, mouseX + 2, mouseY + 12);
	}
	
	public void renderRecipeItems(IRecipe recipe, int posX, int posY) {
		int internalX = posX,
		internalY = posY,
		xCount = 0,
		maxXCount = 3;
		if(recipe instanceof ShapedRecipes) {
			ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;
			if(!(shapedRecipe.getRecipeWidth() > 2 || shapedRecipe.getRecipeHeight() > 2))
				maxXCount = 2;
			for(ItemStack item : shapedRecipe.getRecipeItems()) {
				renderItem(item, internalX, internalY, 0, 0);
				internalX += 22;
				xCount++;
				if(xCount >= maxXCount) {
					internalY += 22;
					internalX = posX;
					xCount = 0;
				}
			}
		} else if(recipe instanceof ShapelessRecipes) {
			ShapelessRecipes shapedRecipe = (ShapelessRecipes) recipe;
			if(shapedRecipe.getRecipeSize() <= 4)
				maxXCount = 2;
			for(Object o : shapedRecipe.getRecipeItems()) {
				ItemStack item = (ItemStack) o;
				renderItem(item, internalX, internalY, 0, 0);
				internalX += 22;
				xCount++;
				if(xCount >= maxXCount) {
					internalY += 22;
					internalX = posX;
					xCount = 0;
				}
			}
		}
	}
		
	private Slot getSlotByStack(ItemStack wantedItem, List<Slot> inventorySlots, int startIndex) {
		if(wantedItem == null)
			return null;
		
		for (int o = startIndex; o < inventorySlots.size(); o++) {
			Slot slot = (Slot) inventorySlots.get(o);
			if(!slot.getHasStack())
				continue;
			if (slot.getStack().itemID == wantedItem.itemID) {
				if(slot.getStack().getItemDamage() != wantedItem.getItemDamage() && wantedItem.getItemDamage() != 32767) {
					continue;
				}
				if(slot.getStack().stackSize < wantedItem.stackSize) {
					continue;
				}
				return slot;
			}
		}
		return null;
	}
		
	private boolean containsInventory(ItemStack item) {
		for (int o = 0; o < tempInventory.size(); o++) {
			ItemStack item1 = (ItemStack)tempInventory.get(o);
			if(item1 == null)
				continue;
			if (item1.itemID == item.itemID) {
				if(item1.getItemDamage() != item.getItemDamage() && item.getItemDamage() != 32767) {
					return false;
				}
				
				item1.stackSize--;
				if(item1.stackSize < 1) {
					tempInventory.remove(item1);
				}
				return true;
			}
		}
		return false;
	}
	
	private List copyInventoryStacks() {
		List copy = new ArrayList();
		for(Object o : mc.thePlayer.inventoryContainer.inventorySlots) {
			if(((Slot)o).getHasStack())
				copy.add(((Slot)o).getStack().copy());
		}
		return copy;
		
	}
	
	
	private void renderItem(ItemStack item, double posX, double posY, int mouseX, int mouseY) {
		RenderItem itemRenderer = new RenderItem();
		mc.theClient.getRenderUtils().drawHalRect(posX, posY, posX + 20, posY + 20, isMouseAround(mouseX, mouseY, posX, posY, 20, 20), false);
		if(item == null)
			return;

		GL11.glPushMatrix();
		RenderHelper.enableGUIStandardItemLighting();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
		itemRenderer.renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, item, (int) posX + 2, (int) posY + 2);
		itemRenderer.renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, item, (int) posX + 2, (int) posY + 2);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}
	
	private boolean isMouseAround(int mouseX, int mouseY, double posX, double posY, double width, double height) {
		return new Bounds(posX, posY, width, height).isPointInside(mouseX, mouseY);
	}
	
	private class FakeGuiInventory extends GuiInventory {

		private AutoCrafting autoCrafting;
		
		public FakeGuiInventory(EntityPlayer player, AutoCrafting autoCrafting) {
			super(player);
			this.autoCrafting = autoCrafting;
		}
		
		public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
			super.mouseClicked(mouseX, mouseY, mouseButton);
			if(selectedRecipe != null)
				new CraftingThread(autoCrafting, selectedRecipe, inventorySlots.inventorySlots, inventorySlots.windowId, 9, true);
	    }
		
		public void initGui() {
			super.initGui();
			this.autoCrafting.findCraftableRecipes(true);
		}
	    
	    public void drawScreen(int par1, int par2, float par3) {
	    	super.drawScreen(par1, par2, par3);
			this.autoCrafting.findCraftableRecipes(true);
	    	autoCrafting.renderCraftableItems(par1, par2, width, height - 40, true);
	    }
		
	}
	
	private class FakeGuiCrafting extends GuiCrafting {

		private AutoCrafting autoCrafting;
		
		public FakeGuiCrafting(EntityPlayer player, World world, int posX, int posY, int posZ, AutoCrafting autoCrafting) {
			super(player.inventory, world, posX, posY, posZ);
			this.autoCrafting = autoCrafting;
		}
		
		public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
			super.mouseClicked(mouseX, mouseY, mouseButton);
			if(selectedRecipe != null)
				new CraftingThread(autoCrafting, selectedRecipe, inventorySlots.inventorySlots, inventorySlots.windowId, 10, false);
		    }
		
		public void initGui() {
			super.initGui();
			this.autoCrafting.findCraftableRecipes(false);
		}
	    
	    public void drawScreen(int par1, int par2, float par3) {
	    	super.drawScreen(par1, par2, par3);
			this.autoCrafting.findCraftableRecipes(false);
	    	autoCrafting.renderCraftableItems(par1, par2, width, height - 40, false);
		}
		
	}
	private class CraftingThread implements Runnable {
		private AutoCrafting autoCrafting;
		private IRecipe recipe;
		private List<Slot> inventorySlots;
		private int startIndex, inventoryID;
		private boolean is2x2;
		
		public CraftingThread(AutoCrafting autoCrafting, IRecipe recipe, List<Slot> inventorySlots, int inventoryID, int startIndex, boolean is2x2) {
			this.autoCrafting = autoCrafting;
			this.recipe = recipe;
			this.inventorySlots = inventorySlots;
			this.startIndex = startIndex;
			this.inventoryID = inventoryID;
			this.is2x2 = is2x2;
			new Thread(this).start();
		}
		
		@Override
		public void run() {
			int slot = 0,
			maxCount = 3,
			count = 0;
			if(recipe instanceof ShapedRecipes) {
				ShapedRecipes shapedRecipe = (ShapedRecipes) recipe;
				if(!(shapedRecipe.getRecipeWidth() > 2 || shapedRecipe.getRecipeHeight() > 2))
					maxCount = 2;
				for(ItemStack item : shapedRecipe.getRecipeItems()) {
					slot++;
					count++;
					if(maxCount == 2 && count > maxCount) {
						if(!is2x2) {
							slot++;
							count = 0;
						}
					}
					Slot recipeSlot  = getSlotByStack(item, inventorySlots, startIndex);
					if(recipeSlot == null)
						continue;
					
					//move item to crafting matrix
					clickSlot(recipeSlot.slotNumber, 0, 0);
					clickSlot(slot, GuiScreen.isShiftKeyDown() ? 0:1, 0);
					if(!GuiScreen.isShiftKeyDown())
						clickSlot(recipeSlot.slotNumber, 0, 0);
				}
			} else if(recipe instanceof ShapelessRecipes) {
				ShapelessRecipes shapedRecipe = (ShapelessRecipes) recipe;
				if(shapedRecipe.getRecipeSize() <= 4)
					maxCount = 2;
				for(Object o : shapedRecipe.getRecipeItems()) {
					ItemStack item = (ItemStack) o;
					slot++;
					count++;
					if(maxCount == 2 && count > maxCount) {
						if(!is2x2) {
							slot++;
							count = 0;
						}
					}
					Slot recipeSlot  = getSlotByStack(item, inventorySlots, startIndex);
					if(recipeSlot == null) {
						System.out.println("couldn't find slot with the item.");
						continue;
					}
					//move item to crafting matrix
					clickSlot(recipeSlot.slotNumber, 0, 0);
					clickSlot(slot, GuiScreen.isShiftKeyDown() ? 0:1, 0);
					if(!GuiScreen.isShiftKeyDown())
						clickSlot(recipeSlot.slotNumber, 0, 0);
				}
			}
			autoCrafting.findCraftableRecipes(is2x2);
		}
		
		private void clickSlot(int slot, int rightMouse, int shiftClick) {
			mc.playerController.windowClick(inventoryID, slot, rightMouse, 0,
					mc.thePlayer);
			try {
				Thread.sleep(ModManager.getNoCheatMode().isState() ? 60L:25L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
