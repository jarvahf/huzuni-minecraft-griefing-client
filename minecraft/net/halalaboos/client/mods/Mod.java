package net.halalaboos.client.mods;

import org.lwjgl.input.Keyboard;
import net.halalaboos.client.Client;
import net.halalaboos.client.base.Binding;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.EventHandler;
import net.halalaboos.client.event.events.EventKeyPressed;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.client.Minecraft;

public abstract class Mod {
	
	private boolean renderIngame, state = false;
	private String name;
	private String description = null;
	public EventHandler eventHandler = Client.getEventHandler();
	private Binding keyBind;
	public Minecraft mc = Client.getMC();
	public WindowCategory category;
	public Type type;
	
	public Mod() {
		this("", -1, false);
	}
	
	public Mod(String name, int keyCode) {
		this(name, keyCode, true);
	}
	
	public Mod(String name, int keyCode, boolean renderIngame) {
		this.name = name;
		this.keyBind = new ModBind(keyCode, this);
		this.renderIngame = renderIngame;
		eventHandler.registerListener(EventKeyPressed.class, keyBind);
	}
	
	public abstract void onToggle();
	
	public abstract void onEnable();

	public abstract void onDisable();

	public void toggle() {
		this.setState(!isState());
		onToggle();
	}
	public boolean isRenderIngame() {
		return renderIngame;
	}
	
	public void setRenderIngame(boolean renderIngame) {
		this.renderIngame = renderIngame;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
		if(state)
			onEnable();
		else
			onDisable();
	}

	public String getKeyName() {
		return getKeyBind().getKeyName();
	}
	
	public Binding getKeyBind() {
		return keyBind;
	}

	public void setKey(int keyCode) {
		this.keyBind.setKeyCode(keyCode);
	}
	
	public WindowCategory getCategory() {
		return category;
	}

	public void setCategory(WindowCategory category) {
		this.category = category;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public enum Type {
		PVP(0x992222),
		PLAYER(0x3399FF),
		WORLD(0xFFFF99),
		RENDER(0xFF6600),
		MOVEMENT(0x009933),
		MINING(0xCC0033),
		FLUID(0x006699)
		;
		private int color;
		
		Type(int color) {
			this.color = color;
		}
		
		public int getColor() {
			return color;
		}
	}
	
	public class ModBind extends Binding {

		private Mod mod;
		
		public ModBind(int keyCode, Mod mod) {
			super(mod.getName(), keyCode);
			this.mod = mod;
		}

		@Override
		public void onPressed() {
			mod.toggle();
		}
		
	}
}
