package net.halalaboos.client.mods.pvp;

import java.util.*;
import org.lwjgl.opengl.*;
import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;
import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.overrides.HEntityClientPlayerMP;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.*;

public class ProjectilePrediction extends Mod implements Listener {

	public ProjectilePrediction() {
		super("Projectiles", -1);
		setType(Type.PVP);
		setDescription("Shows trajectory of throwables.");
	}
	
	
	public void renderIn3D() {
		boolean bow = false;
		HEntityClientPlayerMP player = mc.thePlayer;
		/*check for bow and if the item is throwable*/
		if(player.getCurrentEquippedItem() != null) {
			Item item = player.getCurrentEquippedItem().getItem();
			if(!(item instanceof ItemBow || item instanceof ItemSnowball || item instanceof ItemEnderPearl || item instanceof ItemEgg))
				return;
			if((item instanceof ItemBow)) {
				bow = true;
			}
		}else
			return;
		
		/*copypasta from EntityArrow.java and EntityThrowable.java*/
		double 
		posX = RenderManager.renderPosX - (double)(MathHelper.cos(player.rotationYaw / 180.0F * (float)Math.PI) * 0.16F),
		posY = (RenderManager.renderPosY + (double)player.getEyeHeight()) - 0.10000000149011612D, 
		posZ = RenderManager.renderPosZ - (double)(MathHelper.sin(player.rotationYaw / 180.0F * (float)Math.PI) * 0.16F),
		motionX =  (double)(-MathHelper.sin(player.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float)Math.PI)) * (bow ? 1.0:0.4), 
		motionY = (double)(-MathHelper.sin(player.rotationPitch / 180.0F * (float)Math.PI)) * (bow ? 1.0:0.4), 
		motionZ = (double)(MathHelper.cos(player.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(player.rotationPitch / 180.0F * (float)Math.PI)) * (bow ? 1.0:0.4);
		
        //POWER: ((power ranging from 0 ~ 1) * 2) * 1.5
		/*ItemBow.java*/
		if(player.getItemInUseCount() <= 0 && bow) {
			return;
		}
        int var6 = 72000 - player.getItemInUseCount();
        float power = (float)var6 / 20.0F;
        power = (power * power + power * 2.0F) / 3.0F;
        if ((double)power < 0.1D)
            return;
        if (power > 1.0F)
            power = 1.0F;
		glColor3f((1 - power), power, 0);

		/*prep motion (EntityArrow.java)*/
		float distance = MathHelper.sqrt_double(motionX * motionX + motionY * motionY + motionZ * motionZ);
		motionX /= (double)distance;
		motionY /= (double)distance;
		motionZ /= (double)distance;
		/*power (more EntityArrow.java)*/
		motionX *= (bow ? (power * 2):1) * 1.5;
		motionY *= (bow ? (power * 2):1) * 1.5;
		motionZ *= (bow ? (power * 2):1) * 1.5;
		
		glLineWidth(2);
		glBegin(GL_LINE_STRIP);
		boolean hasLanded = false, isEntity = false;
		MovingObjectPosition landingPosition = null;
		float size = (float) (bow ? 0.3:0.25);
		for(int count = 0; count <= 999 && !hasLanded; count++) {
			/*Check for landing on a block*/
			Vec3 present = mc.theWorld.getWorldVec3Pool().getVecFromPool(posX, posY, posZ);
	        Vec3 future = mc.theWorld.getWorldVec3Pool().getVecFromPool(posX + motionX, posY + motionY, posZ + motionZ);
	        MovingObjectPosition possibleLandingStrip = mc.theWorld.rayTraceBlocks_do_do(present, future, false, true);
	        present = mc.theWorld.getWorldVec3Pool().getVecFromPool(posX, posY, posZ);
	        future = mc.theWorld.getWorldVec3Pool().getVecFromPool(posX + motionX, posY + motionY, posZ + motionZ);
	        if(possibleLandingStrip != null) {
	        	hasLanded = true;
	        	landingPosition = possibleLandingStrip;
	        }
	        /*Check for landing on an entity*/
	        Entity hitEntity = null;
	        AxisAlignedBB arrowBox = new AxisAlignedBB(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
	        List entities = getEntitiesWithinAABB(arrowBox.addCoord(motionX, motionY, motionZ).expand(1.0D, 1.0D, 1.0D));

	        for (int index = 0; index < entities.size(); ++index)
	        {
	            Entity entity = (Entity)entities.get(index);

	            if (entity.canBeCollidedWith() && (entity != player))
	            {
	                float var11 = 0.3F;
	                AxisAlignedBB var12 = entity.boundingBox.expand((double)var11, (double)var11, (double)var11);
	                MovingObjectPosition possibleEntityLanding = var12.calculateIntercept(present, future);
	                if (possibleEntityLanding != null)
	                {
	                    hasLanded = true;
	                    isEntity = true;
	    	        	landingPosition = possibleEntityLanding;
	                }
	            }
	        }
	        /*Arrow rendering and calculation math stuff*/
			posX += motionX;
			posY += motionY;
			posZ += motionZ;
			float motionAdjustment = 0.99F;
			AxisAlignedBB boundingBox = new AxisAlignedBB(posX - size, posY - size, posZ - size, posX + size, posY + size, posZ + size);
	        if (isInMaterial(boundingBox, Material.water))
	        	motionAdjustment = 0.8F;
	        
			motionX *= motionAdjustment;
			motionY *= motionAdjustment;
			motionZ *= motionAdjustment;
			motionY -= bow ? 0.05D:0.03D;
			glVertex3d(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
		}
		glEnd();
		glPushMatrix();
		glTranslated(posX - RenderManager.renderPosX, posY - RenderManager.renderPosY, posZ - RenderManager.renderPosZ);
		if(landingPosition != null) {
				switch(landingPosition.sideHit) {
				case 2://east
					glRotatef(90, 1, 0, 0);
					break;
				case 3://west
					glRotatef(90, 1, 0, 0);
					break;
				case 4://north
					glRotatef(90, 0, 0, 1);
					break;
				case 5://south
					glRotatef(90, 0, 0, 1);
					break;
				default:
					break;
				}
				if(isEntity)
					glColor3f(1, 0, 0);
		}
		renderPoint();
		glPopMatrix();
	}

	public void renderPoint() {
		glBegin(GL_LINES);
		glVertex3d(-.5, 0, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 0, -.5);
		glVertex3d(0, 0, 0);
		
		glVertex3d(.5, 0, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 0, .5);
		glVertex3d(0, 0, 0);
		glEnd();
	
		Cylinder c = new Cylinder();
		glRotatef(-90, 1, 0, 0);
		c.setDrawStyle( GLU.GLU_LINE);
		c.draw(0.5f, 0.5f, 0.1f, 24, 1);
	}
	
    public boolean isInMaterial(AxisAlignedBB par1AxisAlignedBB, Material par2Material)
    {
        int var4 = MathHelper.floor_double(par1AxisAlignedBB.minX);
        int var5 = MathHelper.floor_double(par1AxisAlignedBB.maxX + 1.0D);
        int var6 = MathHelper.floor_double(par1AxisAlignedBB.minY);
        int var7 = MathHelper.floor_double(par1AxisAlignedBB.maxY + 1.0D);
        int var8 = MathHelper.floor_double(par1AxisAlignedBB.minZ);
        int var9 = MathHelper.floor_double(par1AxisAlignedBB.maxZ + 1.0D);

        if (!mc.theWorld.checkChunksExist(var4, var6, var8, var5, var7, var9))
        {
            return false;
        }
        else
        {
            boolean var10 = false;
            Vec3 var11 = mc.theWorld.getWorldVec3Pool().getVecFromPool(0.0D, 0.0D, 0.0D);

            for (int var12 = var4; var12 < var5; ++var12)
            {
                for (int var13 = var6; var13 < var7; ++var13)
                {
                    for (int var14 = var8; var14 < var9; ++var14)
                    {
                        Block var15 = Block.blocksList[mc.theWorld.getBlockId(var12, var13, var14)];

                        if (var15 != null && var15.blockMaterial == par2Material)
                        {
                            double var16 = (double)((float)(var13 + 1) - BlockFluid.getFluidHeightPercent(mc.theWorld.getBlockMetadata(var12, var13, var14)));

                            if ((double)var7 >= var16)
                            {
                                var10 = true;
                            }
                        }
                    }
                }
            }
            return var10;
        }
    }

    public List getEntitiesWithinAABB(AxisAlignedBB par2AxisAlignedBB)
    {
        ArrayList var4 = new ArrayList();
        int var5 = MathHelper.floor_double((par2AxisAlignedBB.minX - 2.0D) / 16.0D);
        int var6 = MathHelper.floor_double((par2AxisAlignedBB.maxX + 2.0D) / 16.0D);
        int var7 = MathHelper.floor_double((par2AxisAlignedBB.minZ - 2.0D) / 16.0D);
        int var8 = MathHelper.floor_double((par2AxisAlignedBB.maxZ + 2.0D) / 16.0D);

        for (int var9 = var5; var9 <= var6; ++var9)
        {
            for (int var10 = var7; var10 <= var8; ++var10)
            {
                if (mc.theWorld.chunkExists(var9, var10))
                {
                	mc.theWorld.getChunkFromChunkCoords(var9, var10).getEntitiesWithinAABBForEntity(mc.thePlayer, par2AxisAlignedBB, var4, (IEntitySelector)null);
                }
            }
        }

        return var4;
    }

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			renderIn3D();
		}
	}


	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRender3D.class, this);

	}


	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRender3D.class, this);

	}

}
