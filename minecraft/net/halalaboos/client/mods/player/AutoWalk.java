package net.halalaboos.client.mods.player;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.halalaboos.client.utils.ClientUtils;
import net.minecraft.src.MathHelper;
import org.lwjgl.input.Keyboard;

public class AutoWalk extends Mod implements Listener {
	
	public AutoWalk() {
		super("AutoWalk", -1);
		setCategory(WindowCategory.PLAYER);
		setType(Type.PLAYER);
		setDescription("Automatically walk.");
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement pEvent = (EventMovement)event;
			if(pEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				if(mc.thePlayer.isCollidedHorizontally && mc.thePlayer.onGround && !mc.thePlayer.handleWaterMovement()){
					mc.thePlayer.jump();
				}
				if (mc.thePlayer.handleWaterMovement() && !ModManager.getMod("Dolphin").isState()) {
					ModManager.getMod("Dolphin").toggle();
				}
			}
			mc.gameSettings.keyBindForward.pressed = true;
		}
	}

	@Override
	public void onToggle() {
	}
	

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);
		
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		mc.gameSettings.keyBindForward.pressed = false;
		
	}
}
