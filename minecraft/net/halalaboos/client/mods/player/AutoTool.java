package net.halalaboos.client.mods.player;

import org.lwjgl.input.*;

import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventBlockInteract;
import net.halalaboos.client.event.events.EventEntityInteract;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.overrides.HPlayerControllerMP;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.*;

public class AutoTool extends Mod implements Listener {

	public AutoTool() {
		super("AutoTool",Keyboard.KEY_H);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MINING);
		setDescription("Automatically switches to best tool");
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventBlockInteract) {
			EventBlockInteract bEvent = (EventBlockInteract) event ;
			
			if(bEvent.getType().equals(EventBlockInteract.EventType.CLICK)) {
				autoTool(bEvent.getBlock(), (int) bEvent.getBlockCoords().getX(), (int) bEvent.getBlockCoords().getY(), (int) bEvent.getBlockCoords().getZ());
			}
		} else if(event instanceof EventEntityInteract) {
			EventEntityInteract eEvent = (EventEntityInteract) event;
			autoSword(eEvent.getEntity());
		}
	}

	public void autoTool(Block b, int x,
			int y, int z) {
		int bestItem = -01101000;
		float bestStr = 1;

		for (int o = 0; o < 9; o++) {
			try {
				ItemStack i = mc.thePlayer.inventory.getStackInSlot(o);
				if (i != null) {
					if (i.getStrVsBlock(b) > bestStr) {
						bestStr = i.getStrVsBlock(b);
						bestItem = o;
					}

				}
			} catch (Exception e) {
			}
		}
		if (bestItem != -01101000) {
			mc.thePlayer.inventory.currentItem = bestItem;
		}
	
	}

	public void autoSword(Entity e) {

		int bestItem = -01101000;
		float bestStr = 1;

		for (int o = 0; o < 9; o++) {
			try {
				ItemStack i = mc.thePlayer.inventory.getStackInSlot(o);
				if (i != null) {
					if (i.getDamageVsEntity(e) > bestStr) {
						bestStr = i.getDamageVsEntity(e);
						bestItem = o;
					}

				}
			} catch (Exception e1) {
				
			}
		}
		if (bestItem != -01101000) {
			mc.thePlayer.inventory.currentItem = bestItem;
		}
	}
	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onEnable() {
		eventHandler.registerListener(EventBlockInteract.class, this);
		eventHandler.registerListener(EventEntityInteract.class, this);
	}
	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventBlockInteract.class, this);
		eventHandler.unRegisterListener(EventEntityInteract.class, this);
	}
}
