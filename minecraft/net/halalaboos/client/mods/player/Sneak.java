package net.halalaboos.client.mods.player;

import org.lwjgl.input.Keyboard;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventMovement;
import net.halalaboos.client.event.events.EventTick;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.Packet19EntityAction;

public class Sneak extends Mod implements Listener {

	public Sneak() {
		super("Sneak", Keyboard.KEY_Z);
		setCategory(WindowCategory.PLAYER);
		setType(Type.MOVEMENT);
		setDescription("Automatically sneaks for the player.");
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventMovement) {
			EventMovement pEvent = (EventMovement)event;
			if(pEvent.getType().equals(EventMovement.EventType.PRE_UPDATE)) {
				if(ModManager.getNoCheatMode().isState())
					mc.gameSettings.keyBindSneak.pressed = true;
			}
		}
	}

	@Override
	public void onToggle() {
		if(isState())
			mc.getNetHandler().addToSendQueue(new Packet19EntityAction(mc.thePlayer, 1));
		else
			mc.getNetHandler().addToSendQueue(new Packet19EntityAction(mc.thePlayer, 2));
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventMovement.class, this);

	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventMovement.class, this);
		if(ModManager.getNoCheatMode().isState())
			mc.gameSettings.keyBindSneak.pressed = false;		
	}

}
