package net.halalaboos.client.mods.render;


import org.lwjgl.input.Keyboard;
import org.lwjgl.util.glu.Cylinder;
import org.lwjgl.util.glu.GLU;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.files.FriendsFile;
import net.halalaboos.client.mods.Mod;
import net.halalaboos.client.mods.Mod.Type;
import net.halalaboos.client.ui.window.WindowCategory;
import net.minecraft.src.*;
import static org.lwjgl.opengl.GL11.*;

public class ChestESP extends Mod implements Listener {
	Cylinder cylinder = new Cylinder();

	public ChestESP() {
		super("Chest ESP", Keyboard.KEY_K);
		setCategory(WindowCategory.RENDER);
		setType(Type.RENDER);
		setDescription("Render box around chests");

	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			for(Object o : mc.theWorld.loadedTileEntityList) {
				if(o instanceof TileEntityChest) {
					TileEntityChest chest = (TileEntityChest) o;
					double renderX = chest.xCoord - RenderManager.renderPosX;
					double renderY = chest.yCoord - RenderManager.renderPosY;
					double renderZ = chest.zCoord - RenderManager.renderPosZ;
					glPushMatrix();
					glTranslated(renderX, renderY, renderZ);
					if(chest.adjacentChestXPos != null) {
						mc.theClient.getRenderUtils().drawCrossedBox
						(new AxisAlignedBB(0, 0, 0, 2, 1, 1), 1, 1, 0.6f, true);
					} else if(chest.adjacentChestZPosition != null) {
						mc.theClient.getRenderUtils().drawCrossedBox
						(new AxisAlignedBB(0, 0, 0, 1, 1, 2), 1, 1, 0.6f, true);
					} else if(chest.adjacentChestXPos == null && chest.adjacentChestZPosition == null && chest.adjacentChestXNeg == null && chest.adjacentChestZNeg == null)
						mc.theClient.getRenderUtils().drawCrossedBox
						(new AxisAlignedBB(0, 0, 0, 1, 1, 1), 1, 1, 0.6f, true);
					glPopMatrix();
				}
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {
		eventHandler.registerListener(EventRender3D.class, this);
	}

	@Override
	public void onDisable() {
		eventHandler.unRegisterListener(EventRender3D.class, this);
		
	}

}
