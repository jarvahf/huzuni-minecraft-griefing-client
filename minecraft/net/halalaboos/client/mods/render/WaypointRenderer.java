package net.halalaboos.client.mods.render;

import static org.lwjgl.opengl.GL11.*;
import net.halalaboos.client.base.Waypoint;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventRender3D;
import net.halalaboos.client.manager.WaypointManager;
import net.halalaboos.client.mods.Mod;
import net.minecraft.src.*;

public class WaypointRenderer extends Mod implements Listener {
	
	public WaypointRenderer() {
		super("Waypoint Renderer", -1, false);
		eventHandler.registerListener(EventRender3D.class, this);
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventRender3D) {
			for(Waypoint waypoint : WaypointManager.getWaypoints()) {
				if(!waypoint.isOnServer())
					continue;
				double renderX = waypoint.getCoordinates().getX() - RenderManager.renderPosX;
				double renderY = waypoint.getCoordinates().getY() - RenderManager.renderPosY;
				double renderZ = waypoint.getCoordinates().getZ() - RenderManager.renderPosZ;
				glColor3d(waypoint.getRed(), waypoint.getGreen(), waypoint.getBlue());
				glPushMatrix();
				glTranslated(renderX, renderY, renderZ);
				
				mc.theClient.getRenderUtils().drawOutlinedBox(new AxisAlignedBB(-0.5, -0.5, -0.5, 0.5, 0.5, 0.5));
				glColor4d(waypoint.getRed(), waypoint.getGreen(), waypoint.getBlue(), 0.1D);
				mc.theClient.getRenderUtils().drawBox(new AxisAlignedBB(-0.5, -0.5, -0.5, 0.5, 0.5, 0.5));
				glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		        glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
				glScalef(-(0.016666668F * 1.6F), -(0.016666668F * 1.6F), (0.016666668F * 1.6F));
				
				glTranslatef(0, -32, 0);
				double distance = mc.theClient.getClientUtils().getDistance(waypoint.getCoordinates().getX(), waypoint.getCoordinates().getY(), waypoint.getCoordinates().getZ());
				if((distance / 6) > 1)
					glScaled((distance / 6), (distance / 6), (distance / 6));
				glColor4f(0, 0, 0, 0.5F);
				mc.theClient.getRenderUtils().drawRect(-mc.fontRenderer.getStringWidth(waypoint.getName()) / 2 - 2, -1, mc.fontRenderer.getStringWidth(waypoint.getName()) / 2 + 2, 11);
				glEnable(GL_TEXTURE_2D);
		        mc.fontRenderer.drawStringWithShadow(waypoint.getName(), -mc.fontRenderer.getStringWidth(waypoint.getName()) / 2, 0, 0xFFFFFF);
				glDisable(GL_TEXTURE_2D);

				glPopMatrix();
			}
		}
	}

	@Override
	public void onToggle() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnable() {

	}

	@Override
	public void onDisable() {
		// TODO Auto-generated method stub
		
	}

}
