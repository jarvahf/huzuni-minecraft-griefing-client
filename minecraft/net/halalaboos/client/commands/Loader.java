package net.halalaboos.client.commands;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;

public class Loader extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "load" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".load" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		mc.theClient.getModLoader().loadExternalMods(mc);
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Reloads all external mods for Huzuni.";
	}
}
