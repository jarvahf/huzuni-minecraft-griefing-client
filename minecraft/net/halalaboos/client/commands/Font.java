package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.utils.font.CFontRenderer;

public class Font extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "font" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".font <size> <fontName>" };
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		String fontName = originalString.split(args[0] + " " + args[1] + " ")[1];
		int fontSize = Integer.parseInt(args[1]);
		mc.theClient.setFR(new CFontRenderer(mc, fontName, fontSize));
		mc.theClient.addChatMessage("Font set to '" + fontName + "' with size '" + fontSize + "'");
	}
	
	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Change the custom font's type and size.";
	}
	
}
