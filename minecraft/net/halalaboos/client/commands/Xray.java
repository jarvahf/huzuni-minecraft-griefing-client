package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.XrayFile;
import net.halalaboos.client.manager.ModManager;
import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;

public class Xray extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] { "xray" };
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] { ".xray add/remove <block name/block ID>" };
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Add or remove blocks from the xray mod.";
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		
		if(args[1].startsWith("a")) {
			String blockName = originalString.split(args[0] + " " + args[1] + " ")[1];
			Block block = getBlockByName(blockName);
			if(block != null) {
			if(XrayFile.add(block.blockID))
				mc.theClient.addChatMessage("'" + blockName + "' added to xray!");
			else
				mc.theClient.addChatMessage("'" + blockName + "' already in xray!");
			if(ModManager.getMod("Xray").isState())
				mc.renderGlobal.loadRenderers();
			} else
				mc.theClient.addChatMessage("'" + blockName + "' is not a valid block!");
		} else if(args[1].startsWith("r")) {
			String blockName = originalString.split(args[0] + " " + args[1] + " ")[1];
			Block block = getBlockByName(blockName);
			if(block != null) {
			if(XrayFile.remove(block.blockID))
				mc.theClient.addChatMessage("'" + blockName + "' removed from xray!");
			else
				mc.theClient.addChatMessage("'" + blockName + "' is not in xray!");
			if(ModManager.getMod("Xray").isState())
				mc.renderGlobal.loadRenderers();
			} else
				mc.theClient.addChatMessage("'" + blockName + "' is not a valid block!");
		}
		
	}

	public Block getBlockByName(String name) {
		if(isInteger(name)) {
			int blockID = Integer.parseInt(name);
			for(Block block : Block.blocksList) {
				if(block == null)
					continue;
				if(block.blockID == blockID)
					return block;
			}
		} else {
			for(Block block : Block.blocksList) {
				if(block == null)
					continue;
				ItemStack item = new ItemStack(block);
				if(item.getDisplayName().equalsIgnoreCase(name))
					return block;
			}
		}
		return null;
	}
	
	public boolean isInteger(String text) {
		try {
		Integer.parseInt(text);
		return true;
		} catch (Exception e) {
		
		}
		return false;
	}
}
