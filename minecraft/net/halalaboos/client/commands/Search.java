package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.files.SearchFile;
import net.halalaboos.client.files.XrayFile;
import net.halalaboos.client.manager.ModManager;
import net.minecraft.src.Block;
import net.minecraft.src.ItemStack;

public class Search extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] {"search"};
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] {".search add/remove block name/block ID"};
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Add blocks to your list of blocks in the search mod.";
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		
		if(args[1].startsWith("a")) {
			String blockName = originalString.split(args[0] + " " + args[1] + " ")[1];
			Block block = getBlockByName(blockName);
			if(block != null) {
			if(SearchFile.add(block.blockID))
				mc.theClient.addChatMessage("'" + blockName + "' added to search!");
			else
				mc.theClient.addChatMessage("'" + blockName + "' already in search!");
			if(ModManager.getMod("Search").isState())
				mc.renderGlobal.loadRenderers();
			} else
				mc.theClient.addChatMessage("'" + blockName + "' is not a valid block!");
		} else if(args[1].startsWith("r")) {
			String blockName = originalString.split(args[0] + " " + args[1] + " ")[1];
			Block block = getBlockByName(blockName);
			if(block != null) {
			if(SearchFile.remove(block.blockID))
				mc.theClient.addChatMessage("'" + blockName + "' removed from search!");
			else
				mc.theClient.addChatMessage("'" + blockName + "' is not in search!");
			if(ModManager.getMod("Search").isState())
				mc.renderGlobal.loadRenderers();
			mc.renderGlobal.loadRenderers();
			} else
				mc.theClient.addChatMessage("'" + blockName + "' is not a valid block!");
		}
		
	}

	public Block getBlockByName(String name) {
		if(isInteger(name)) {
			int blockID = Integer.parseInt(name);
			for(Block block : Block.blocksList) {
				if(block == null)
					continue;
				if(block.blockID == blockID)
					return block;
			}
		} else {
			for(Block block : Block.blocksList) {
				if(block == null)
					continue;
				ItemStack item = new ItemStack(block);
				if(item.getDisplayName().equalsIgnoreCase(name))
					return block;
			}
		}
		return null;
	}
	
	public boolean isInteger(String text) {
		try {
		Integer.parseInt(text);
		return true;
		} catch (Exception e) {
		
		}
		return false;
	}
}
