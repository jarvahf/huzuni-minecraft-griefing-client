package net.halalaboos.client.commands;

import net.halalaboos.client.base.Command;
import net.halalaboos.client.manager.WaypointManager;

public class Waypoint extends Command {

	@Override
	public String[] getAliases() {
		// TODO Auto-generated method stub
		return new String[] {"waypoint", "wp"};
	}

	@Override
	public String[] getCommandHelp() {
		// TODO Auto-generated method stub
		return new String[] {".waypoint add <name>", ".waypoint remove <name>", ".waypoint list"};
	}

	@Override
	public void runCommand(String originalString, String[] args) {
		if(args[1].startsWith("a")) {
			
			String waypointName = originalString.split(args[0] + " " + args[1] + " ")[1];
			WaypointManager.addAtEntityPos(waypointName, mc.thePlayer);
			mc.theClient.addChatMessage("Waypoint '\247o" + waypointName + "\247r' set!");

		} else if(args[1].startsWith("r")) {
			
			String waypointName = originalString.split(args[0] + " " + args[1] + " ")[1];
			WaypointManager.remove(waypointName);
			
			mc.theClient.addChatMessage("'\247o" + waypointName + "\247r' removed from waypoints.");
		} else if(args[1].startsWith("l")) {
			
			mc.theClient.addChatMessage("Waypoints on current server: ");
			for(net.halalaboos.client.base.Waypoint w : WaypointManager.getWaypoints()) {
				if(w.isOnServer())
					mc.theClient.addChatMessage("\247a" + w.getName() + " \247f(\247a" + (int) w.getCoordinates().getX() + "\247f, \247a" + (int) w.getCoordinates().getY() + "\247f, \247a" + (int) w.getCoordinates().getZ() + "\247f)");
			}
		}
	}

	@Override
	public String getCommandDescription() {
		// TODO Auto-generated method stub
		return "Add/remove waypoints.";
	}
}
