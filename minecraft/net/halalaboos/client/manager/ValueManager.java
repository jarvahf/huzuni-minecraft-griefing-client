package net.halalaboos.client.manager;

import java.util.ArrayList;
import java.util.List;

import net.halalaboos.client.base.Manager;
import net.halalaboos.client.base.Value;

public class ValueManager extends Manager {

	private static List<Value> values = new ArrayList<Value>();

	
	@Override
	public void onStartup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEnd() {
		// TODO Auto-generated method stub
		
	}

	public static List<Value> getValues() {
		return values;
	}
	
	public static void add(Value val) {
		getValues().add(val);
	}
}
