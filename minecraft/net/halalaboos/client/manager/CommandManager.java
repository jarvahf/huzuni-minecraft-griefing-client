package net.halalaboos.client.manager;

import java.util.*;
import net.halalaboos.client.base.Command;
import net.halalaboos.client.base.Manager;
import net.halalaboos.client.commands.*;
import net.halalaboos.client.event.Event;
import net.halalaboos.client.event.Listener;
import net.halalaboos.client.event.events.EventSendMessage;
import net.halalaboos.client.files.HFile;

public class CommandManager extends Manager implements Listener {
	
	private static List<Command> commands = new ArrayList<Command>();

	@Override
	public void onStartup() {
		mc.theClient.getEventHandler().registerListener(EventSendMessage.class, this);
		commands.add(new Help());
		commands.add(new Add());
		commands.add(new Font());
		commands.add(new Debug());
		commands.add(new Bind());
		commands.add(new Remove());
		commands.add(new Username());
		commands.add(new Loader());
		commands.add(new Gui());
		commands.add(new Enchant());
		commands.add(new Waypoint());
		commands.add(new GetIP());
		commands.add(new Xray());
		commands.add(new Search());
	}

	@Override
	public void onEnd() {
		// TODO Auto-generated method stub
		
	}

	public static void add(Command command) {
		for(Command cmd : commands) {
			if(cmd.getAliases()[0].equalsIgnoreCase(command.getAliases()[0]))
				return;
		}
		commands.add(command);
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof EventSendMessage) {
			EventSendMessage mEvent = (EventSendMessage) event;
			if(mEvent.getMessage().startsWith(".")) {
				mEvent.setCancelled(true);
				for(Command com : commands) {
					for(String alias : com.getAliases()) {
						if(mEvent.getMessage().substring(1).startsWith(alias)) {
							try {
							com.runCommand(mEvent.getMessage().substring(1), mEvent.getMessage().substring(1).split(" "));
							} catch (Exception e) {
								for(String help : com.getCommandHelp())
									mc.theClient.addChatMessage(help);
							}
							return;
						}
					}
				}
				mc.theClient.addChatMessage("Command not recognized. Try '.help'.");
			}
		}
	}

	public static List<Command> getCommands() {
		return commands;
	}

}
