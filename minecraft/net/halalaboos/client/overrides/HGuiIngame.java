package net.halalaboos.client.overrides;

import org.lwjgl.opengl.GL11;

import net.halalaboos.client.event.events.EventRender;
import net.minecraft.client.Minecraft;
import net.minecraft.src.*;

public class HGuiIngame extends GuiIngame {

	private Minecraft mc;
	
	public HGuiIngame(Minecraft par1Minecraft) {
		super(par1Minecraft);
		mc = par1Minecraft;
		// TODO Auto-generated constructor stub
	}

	public void renderGameOverlay(float par1, boolean par2, int par3, int par4) {
		super.renderGameOverlay(par1, par2, par3, par4);
		if(mc.currentScreen == null && !mc.gameSettings.showDebugInfo) {
			mc.theClient.getEventHandler().call(new EventRender(this));
		}
	}
}