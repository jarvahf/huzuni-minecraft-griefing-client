package net.halalaboos.client.utils;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import net.halalaboos.client.Client;

public class Texture {
	private int texID;
	
	public Texture(String texture) {
		texID = Client.getMC().renderEngine.getTexture(texture);
	}
	
	public void renderTexture(double x, double y, double width, double height, Color c) {
		GL11.glColor3f((float)c.getRed() / 255F, (float)c.getGreen() / 255F, (float)c.getBlue() / 255F);
		TextureUtils.renderTexture(texID, x, y, width, height);
	}

	public int getTexID() {
		return texID;
	}

	public void setTextue(String texture) {
		texID = Client.getMC().renderEngine.getTexture(texture);
	}
	
}
