package net.halalaboos.client.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarFile;

import net.halalaboos.client.Client;
import net.halalaboos.client.manager.ModManager;
import net.halalaboos.client.mods.Mod;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiButton;

import org.lwjgl.Sys;

public class ThreadDownload implements Runnable {
	private URL downloadURL;
	private File outputFile;
	private float percentage = 0;
	private boolean installToClient;
	
	public ThreadDownload(URL downloadURL, File outputFile, boolean installToClient) {
		this.downloadURL = downloadURL;
		this.outputFile = outputFile;
		this.installToClient = installToClient;
		new Thread(this).start();
	}
	
	@Override
	public void run() {
		    try {
			BufferedInputStream in = new BufferedInputStream(downloadURL.openStream());
			OutputStream  out = new BufferedOutputStream(new FileOutputStream(outputFile));
			byte data[] = new byte[1024];
		    int count;
			HttpURLConnection connection = (HttpURLConnection) downloadURL.openConnection();
			int filesize = connection.getContentLength();
			connection.disconnect();
			float progress = 0;
		    while((count = in.read(data, 0, 1024)) >= 0)
		    {
		    	progress += count;
		        out.write(data, 0, count);
		        percentage = (progress) / filesize;
		        try {
					Thread.sleep(1L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    }
		    if(in != null)
		    	in.close();
		    if(in != null)
		    	out.close();
		    if(installToClient) {
		    	install();
		    }
		    } catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	private void install() {
		try {
			Minecraft mc = Client.getMC();
			String classPath = mc.theClient.getModLoader().getJarFileClassPath(outputFile, new JarFile(outputFile));
			Mod mod = mc.theClient.getModLoader().loadMod(outputFile, classPath != null ? classPath:"net.halalaboos.client.mods.external.ExternalMod");
			ModManager.load(mod);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public URL getDownloadURL() {
		return downloadURL;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public float getPercentage() {
		return percentage;
	}

}
