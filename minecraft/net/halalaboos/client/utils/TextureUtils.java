package net.halalaboos.client.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import net.halalaboos.client.Client;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.util.glu.GLU;

import static org.lwjgl.opengl.GL11.*;

public class TextureUtils {

	/**
	 * Credits found at
	 * 'http://stackoverflow.com/questions/10801016/lwjgl-textures-and-strings' for
	 * image loading.
	 * 
	 * */
	/*private static final int BYTES_PER_PIXEL = 4;
	public static int loadTexture(BufferedImage image) {

		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0,
				image.getWidth());

		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth()
				* image.getHeight() * BYTES_PER_PIXEL);

		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				int pixel = pixels[y * image.getWidth() + x];
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));
			}
		}

		buffer.flip();

		int textureID = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(),
				image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		return textureID;
	}

	public static BufferedImage loadImage(String loc) {
		try {
			return ImageIO.read(TextureUtils.class.getResource(loc));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	public static void renderTexture(int texID, double x, double y, double width, double height) {
		boolean tex2D = glGetBoolean(GL_TEXTURE_2D);
		boolean blend = glGetBoolean(GL_BLEND);
		glPushMatrix();
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glScalef(0.5F, 0.5F, 0.5F);
		x *= 2;
		y *= 2;
		width *= 2;
		height *= 2;
		glBindTexture(GL_TEXTURE_2D, texID);
		
		glBegin(GL_TRIANGLES);
		glTexCoord2f(1, 0);
		glVertex2d(x + width, y);
		glTexCoord2f(0, 0);
		glVertex2d(x, y);
		glTexCoord2f(0, 1);
		glVertex2d(x, y + height);
		glTexCoord2f(0, 1);
		glVertex2d(x , y + height);
		glTexCoord2f(1, 1);
		glVertex2d(x + width, y + height);
		glTexCoord2f(1, 0);
		glVertex2d(x + width, y);
		glEnd();
		if(!tex2D)
			glDisable(GL_TEXTURE_2D);
		if(!blend)
			glDisable(GL_BLEND);
		glPopMatrix();
	}
}
