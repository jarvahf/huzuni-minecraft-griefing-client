package net.halalaboos.client.utils;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import org.lwjgl.input.Mouse;
import net.halalaboos.client.Client;
import net.minecraft.client.Minecraft;
import net.minecraft.src.Chunk;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.MathHelper;
import net.minecraft.src.ScaledResolution;

public class ClientUtils {
	private Minecraft mc;
	
	public ClientUtils(Minecraft mc) {
		this.mc = mc;
	}
	
	public static Long getSystemTime() {
		return System.nanoTime() / 1000000L;
	}
	
	public File getSaveDirectory() {
		File sFile = new File(mc.getMinecraftDir(), Client.CLIENT_TITLE);
		if(!sFile.exists())
			sFile.mkdirs();
		return sFile;
	}
	
	public float[] getRotation(double posX, double posY, double posZ) {
		Entity p = mc.thePlayer;
        double xDiff = posX - p.posX;
        double zDiff = posZ - p.posZ;
        double yDiff = posY - (p.posY + (double)p.getEyeHeight());
        double distance = (double)MathHelper.sqrt_double(xDiff * xDiff + zDiff * zDiff);
        float yaw = (float)(Math.atan2(zDiff, xDiff) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float)(-(Math.atan2(yDiff, distance) * 180.0D / Math.PI));
        return new float[] { updateRotation(p.rotationYaw, yaw, 100000), updateRotation(p.rotationPitch, pitch, 100000)};
	}
	
	public double getDistance(double x, double y, double z) {
        float xDiff = (float)(mc.thePlayer.posX - x);
        float yDiff = (float)(mc.thePlayer.posY - y);
        float zDiff = (float)(mc.thePlayer.posZ - z);
        return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}
	
	public String getFPS(){
		return mc.debug.split(" ")[0];
	}
	
	public int getFPSint(){
		return mc.debugFPS;
	}
	
    private float updateRotation(float par1, float par2, float par3)
    {
        float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);

        if (var4 > par3)
        {
            var4 = par3;
        }

        if (var4 < -par3)
        {
            var4 = -par3;
        }

        return par1 + var4;
    }


	/**
	 * @return Mouse X.
	 * */
	public int getMouseX() {
		return Mouse.getEventX() * getScreenWidth()
				/ mc.displayWidth;
	}

	/**
	 * @return Mouse Y.
	 * */
	public int getMouseY() {
		return getScreenHeight() - Mouse.getEventY() * getScreenHeight()
				/ mc.displayHeight - 1;
	}

	/**
	 * @return Screen width.
	 * */
	public int getScreenWidth() {
		return getScaledResolution().getScaledWidth();
	}

	/**
	 * @return Screen height.
	 * */
	public int getScreenHeight() {
		return getScaledResolution().getScaledHeight();
	}

	/**
	 * @return a new ScaledResolution instance.
	 * */
	public ScaledResolution getScaledResolution() {
		return new ScaledResolution(mc.gameSettings,
				mc.displayWidth,
				mc.displayHeight);
	}
	
	/**
	 * @return Current date.
	 * */
	public String getDate() {
		SimpleDateFormat d = new SimpleDateFormat("dd/MM/yy");
		return (d.format(new Date()));
	}

	/**
	 * @return Current formatted time.
	 * */
	public String getTime() {
		SimpleDateFormat d = new SimpleDateFormat("h:mm a");
		return (d.format(new Date()));
	}

	/**
	 * @return random boolean.
	 * */
	public boolean getRandomFlag() {
		//	return ((new Random().nextInt() % 2) == 0);
		return new Random().nextBoolean();
	}

	public int getDirection() {
		return MathHelper
				.floor_double((double) (mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
	}
	
	public String getBiome() {
		int x = MathHelper.floor_double(this.mc.thePlayer.posX);
        int z = MathHelper.floor_double(this.mc.thePlayer.posZ);
        Chunk currentChunk = this.mc.theWorld.getChunkFromBlockCoords(x, z);
        return currentChunk.getBiomeGenForWorldCoords(x & 15, z & 15, this.mc.theWorld.getWorldChunkManager()).biomeName;
	}
	
	/**
	 * @return 0 if you're good to go, 1 if an update is available, and -1 if you could not connect to the webserver.
	 * */
	public int isClientUpToDate() {
		String lVersion = getLatestVersion();
		return lVersion.equals("Could not connect to webserver.") ? -1:Client.CLIENT_VERSION.equalsIgnoreCase(getLatestVersion()) ? 0:1;
	}
	
	public String getLatestVersion() {
		try {
			Connection c = new Connection(new URL("http://halalaboos.net/client/version"));
			c.createConnection();
			for(String s : c.readURL()) {
				return s.trim();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "Could not connect to webserver.";
		} catch (IOException e) {
			e.printStackTrace();
			return "Could not connect to webserver.";
		}
		return Client.CLIENT_VERSION;
	}
	
	public Color getRandomColor() {
		Random ra = new Random();
		float r = (float) (ra.nextInt(255) + 1) / 255f;
		float g = (float) (ra.nextInt(255) + 1) / 255f;
		float b = (float) (ra.nextInt(255) + 1) / 255f;
		return new Color(r, g, b);
	}
	
	public Color getMouseColor() {
		try {
			Robot r = new Robot();
			Point point = MouseInfo.getPointerInfo().getLocation();
			return r.getPixelColor((int) point.getX(), (int) point.getY());
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Color.WHITE;
	}
	/**
	 * Credits to 'http://www.mkyong.com/'.
	 * @throws IOException 
	 * */
	public File[] extractZipFile(File zipFile, File destination) throws IOException {
			if(!destination.exists())
				destination.mkdir();
						
	     	byte[] buffer = new byte[1024];
	     	
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			
			ZipEntry entry = zis.getNextEntry();
			
	    	while(entry != null) {
	    		
	    		File newFile = new File(destination + File.separator + entry.getName());
	            new File(newFile.getParent()).mkdirs();
	            
	            FileOutputStream fos = new FileOutputStream(newFile);             
	  
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	            	fos.write(buffer, 0, len);
	            }
	            
	            fos.close();   
	            entry = zis.getNextEntry();
	            
	    	}
	    	zis.closeEntry();
	    	zis.close();
	    	return destination.listFiles();
	}
	
	public boolean isAirNearBlock(int x, int y, int z) {
		if (mc.theWorld.getBlockId(x + 1, y, z) == 0
				|| mc.theWorld.getBlockId(x - 1, y, z) == 0
				|| mc.theWorld.getBlockId(x, y, z + 1) == 0
				|| mc.theWorld.getBlockId(x, y, z - 1) == 0
				|| mc.theWorld.getBlockId(x, y + 1, z) == 0
				|| mc.theWorld.getBlockId(x, y - 1, z) == 0)
			return true;
		else
			return false;
	}
}
