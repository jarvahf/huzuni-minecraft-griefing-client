package net.halalaboos.client.base;

import java.util.*;
import net.halalaboos.client.Client;
import net.minecraft.client.Minecraft;

public abstract class Manager {
	public Minecraft mc = Client.getMC();
	private static List<Manager> managers = new ArrayList<Manager>();
	
	public Manager() {
		managers.add(this);
	}
	public abstract void onStartup();
	public abstract void onEnd();

	public static List<Manager> getManagers() {
		return managers;
	}

	public static void setManagers(List<Manager> managers) {
		Manager.managers = managers;
	}
	
	
	
}
