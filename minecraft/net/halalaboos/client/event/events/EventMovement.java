package net.halalaboos.client.event.events;

import net.halalaboos.client.event.EventCancellable;



public class EventMovement extends EventCancellable {

	private EventType type;
	private double motionX, motionY, motionZ;
	public enum EventType {
		PRE_UPDATE,
		POST_UPDATE,
		MOVE_ENTITY
		;
	}
	
	public EventMovement(Object source, EventType type) {
		super(source);
		this.type = type;
	}
	
	public EventMovement(Object source, double motionX, double motionY, double motionZ) {
		super(source);
		this.motionX = motionX;
		this.motionY = motionY;
		this.motionZ = motionZ;
		this.type = EventType.MOVE_ENTITY;
	}

	public double getMotionX() {
		return motionX;
	}

	public void setMotionX(double motionX) {
		this.motionX = motionX;
	}

	public double getMotionY() {
		return motionY;
	}

	public void setMotionY(double motionY) {
		this.motionY = motionY;
	}

	public double getMotionZ() {
		return motionZ;
	}

	public void setMotionZ(double motionZ) {
		this.motionZ = motionZ;
	}

	public EventType getType() {
		return type;
	}
	
}
