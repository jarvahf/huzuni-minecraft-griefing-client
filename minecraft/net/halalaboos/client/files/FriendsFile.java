package net.halalaboos.client.files;

import java.util.*;
import net.halalaboos.client.base.Friend;
import net.minecraft.src.StringUtils;

public class FriendsFile extends HFile {

	private static List<Friend> friends = new ArrayList<Friend>();
	
	public FriendsFile() {
		super("Protection");
		// TODO Auto-generated constructor stub
	}

	public static boolean add(String username, String alias) {
		username = StringUtils.stripControlCodes(username);
		alias = StringUtils.stripControlCodes(alias);
		remove(alias);
		remove(username);
		return friends.add(new Friend(username, alias));
	}
	
	public static boolean remove(String usernamealias) {
		usernamealias = StringUtils.stripControlCodes(usernamealias);
		for(Friend f : friends) {
			if(f.getUsername().equalsIgnoreCase(usernamealias) || f.getAlias().equalsIgnoreCase(usernamealias))
				return friends.remove(f);
		}
		return false;
	}
	
	public static boolean contains(String usernamealias) {
		usernamealias = StringUtils.stripControlCodes(usernamealias);
		for(Friend f : friends) {
			if(f.getUsername().equalsIgnoreCase(usernamealias) || f.getAlias().equalsIgnoreCase(usernamealias))
				return true;
			
		}
		return false;
	}
	
	public static String replaceNames(String original, boolean replaceWithAlias, String prefix, boolean colorCode) {
		String finalString = original;
		boolean found = false;
		
		for(Friend friend : FriendsFile.getFriends()) {
			String stringReplacement = "";
			for(int index = 0; index < finalString.split(" ").length; index++) {
				String word = finalString.split(" ")[index];
				
				if(word.startsWith(prefix)) {
				
					if(replaceWithAlias) {
						
						if(StringUtils.stripControlCodes(word).toLowerCase().contains(friend.getUsername().toLowerCase())) {
							word = StringUtils.stripControlCodes(word).substring(prefix.length()).toLowerCase().replaceAll(friend.getUsername().toLowerCase(), colorCode ? ("\2479" + friend.getAlias() + "\247f"):friend.getAlias());
						}
						
					} else {
						
						if(StringUtils.stripControlCodes(word).toLowerCase().contains(friend.getAlias().toLowerCase())) {
							word = StringUtils.stripControlCodes(word).substring(prefix.length()).toLowerCase().replaceAll(friend.getAlias().toLowerCase(), colorCode ? ("\2479" + friend.getUsername() + "\247f"):friend.getUsername());
						}
						
					}
				}
				if(index != original.split(" ").length - 1)
					stringReplacement += word + " ";
				else
					stringReplacement += word;
			}
			finalString = stringReplacement;
		}
		return finalString;
	}
	
	@Override
	public void onStart() {
		for(String s : readFile()) {
			String s1[] = s.split(":");
			friends.add(new Friend(s1[0], s1[1]));
		}
	}

	@Override
	public void onEnd() {
		List<String> words = new ArrayList<String>();
		for(Friend friend : friends)
			words.add(friend.getUsername() + ":" + friend.getAlias());
		saveFile(words);
	}

	public static List<Friend> getFriends() {
		return friends;
	}

}
