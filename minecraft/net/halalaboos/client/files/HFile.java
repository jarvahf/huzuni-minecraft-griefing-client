package net.halalaboos.client.files;

import java.io.*;
import java.util.*;

import net.halalaboos.client.Client;
import net.halalaboos.client.utils.ClientUtils;

public abstract class HFile {

	private File saveFile;
	
	public HFile(String fileName) {
		saveFile = new File(Client.getClientUtils().getSaveDirectory(), fileName);
		if(!saveFile.exists()) {
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public String getName() {
		return saveFile.getName();
	}
	
	public abstract void onStart();
	public abstract void onEnd();

	
	public List<String> readFile() {
		List<String> l = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(saveFile));
			for(String s; (s = reader.readLine()) != null;) {
				if(!s.startsWith("#"))
					l.add(s.trim());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
	
	public void saveFile(List<String> list) {
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(saveFile));
			for(String s : list) {
				writer.println(s);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
